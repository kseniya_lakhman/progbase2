#include "list.h"

static void List_InvalidIndexError()
{
    assert(0 && "Invalid index");
    fprintf(stderr, "Invalid index");
}
//
static void List_realloc(List *self, size_t newCapacity)
{
    float *newArray = realloc(self->items, sizeof(self->items[0]) * newCapacity);
    if (newArray == NULL)
    {
        fprintf(stderr, "Reallocation error\n");
        abort();
    }
    self->items = newArray;
    self->capacity = newCapacity;
    // printf("Reallocated\n");
}
void List_init(List *self)
{
    self->capacity = 4;
    self->size = 0;
    self->items = malloc(sizeof(self->items[0]) * self->capacity);
    if (self->items == NULL)
    {
        fprintf(stderr, "Allocation error\n");
        abort();
    }
}
//
void List_deinit(List *self)
{
    free(self->items);
}
//
List *List_alloc(void)
{
    List *self = malloc(sizeof(List));
    List_init(self);
    return self;
}
//
void List_free(List *self)
{
    List_deinit(self);
    free(self);
}
//
size_t List_size(List *self)
{
    return self->size;
}
//
float List_get(List *self, int index)
{
    if (index < 0 || index > List_size(self))
    {
        List_InvalidIndexError();
        return -1;
    }
    return self->items[index];
}
void List_set(List *self, int index, float value)
{
    if (index < 0 || index > List_size(self))
    {
        List_InvalidIndexError();
        return;
    }
    self->items[index] = value;
}
//
void List_insert(List *self, int index, float value)
{
    if (index < 0 || index > List_size(self))
    {
        List_InvalidIndexError();
        return;
    }

    if (self->size == self->capacity)
    {
        int newCapacity = self->capacity * 2;
        List_realloc(self, newCapacity);
    }

    for (int i = self->size - 1; i >= index; i--)
    {
        self->items[i + 1] = self->items[i];
    }
    self->items[index] = value;
    self->size += 1;
}
//
void List_removeAt(List *self, int index)
{
    if (index < 0 || index > List_size(self))
    {
        List_InvalidIndexError();
        return;
    }

    for (int i = index; i < List_size(self); i++)
    {
        self->items[i] = self->items[i + 1];
    }
    self->size -= 1;
}
//
void List_add(List *self, float value)
{
    if (self->size == self->capacity)
    {
        int newCapacity = self->capacity * 2;
        List_realloc(self, newCapacity);
    }
    self->items[self->size] = value;
    self->size += 1;
}
//
void List_remove(List *self, float value)
{
    List_removeAt(self, List_indexOf(self, value));
}
//
int List_indexOf(List *self, float value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
        {
            return i;
        }
    }
    return -1;
}
//
bool List_contains(List *self, float value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
        {
            return true;
        }
    }
    return false;
}
//
bool List_isEmpty(List *self)
{
    return List_size(self) == 0;
}
//
void List_clear(List *self)
{
    self->size = 0;
}
//