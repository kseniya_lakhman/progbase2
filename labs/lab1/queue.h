#pragma once
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct __Queue Queue;

struct __Queue
{
    float *items;
    int capacity;
    int first;
    int last;
};

Queue *Queue_alloc(void);
void Queue_free(Queue *self);

void Queue_init(Queue *self);
void Queue_deinit(Queue *self);

size_t Queue_size(Queue *self);

void Queue_enqueue(Queue *self, float value);
float Queue_dequeue(Queue *self);

bool Queue_isEmpty(Queue *self);