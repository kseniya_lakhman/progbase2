#include "queue.h"
#include "list.h"
#include <progbase/console.h>
#include <math.h>

void List_print(List *self);
void Queue_print(Queue *self);

void List_testing();
void Queue_testing();

int main()
{
    //
    List_testing();
    Queue_testing();
    Console_clear();
    //
    puts(" >>>--------------------List-----------------------\n");

    List *list;
    list = List_alloc();

    //reading from file
    char buf[100];
    int buf_i = 0;

    FILE *fin = fopen("data.txt", "r");
    char ch = fgetc(fin);

    while (1)
    {
        if (ch == ' ' || ch == EOF)
        {
            buf[buf_i] = '\0';
            buf_i = 0;
            float curr_num = atof(buf);
            if (buf[0] != '\0')
            {
                List_add(list, curr_num);
            }
        }
        else
        {
            buf[buf_i++] = ch;
        }
        if (ch == EOF)
        {
            break;
        }
        ch = fgetc(fin);
    }
    if (fin != NULL)
    {
        fclose(fin);
    }
    //

    puts("\n--------------->>First list----------------------");
    List_print(list);

    float num = 0;
    int firts_position = 0;
    int length = List_size(list);
    for (int i = 0; i < length; i++)
    {
        num = List_get(list, i);
        if (fabs(num) > 10)
        {
            List_removeAt(list, i);
            List_insert(list, firts_position, num);
        }
    }
    puts("\n--------------->>Numbers greater than 10 moved to the top of list------");
    List_print(list);

    puts("\n >>>--------------------Queue-----------------------\n");

    Queue *q1;
    Queue *q2;
    Queue *queue;

    q1 = Queue_alloc();
    q2 = Queue_alloc();
    queue = Queue_alloc();

    for (int i = 0; i < List_size(list); i++)
    {
        if (i % 2 == 0)
        {
            Queue_enqueue(q2, List_get(list, i));
        }
        else
        {
            Queue_enqueue(q1, List_get(list, i));
        }
    }

    puts("-----------Queue1---------");
    Queue_print(q1);
    puts("\n-----------Queue2---------");
    Queue_print(q2);

    int length1 = Queue_size(q1);
    for (int i = 0; i < length1; i++)
    {
        Queue_enqueue(queue, Queue_dequeue(q1));
    }

    int length2 = Queue_size(q2);
    for (int i = 0; i < length2; i++)
    {
        Queue_enqueue(queue, Queue_dequeue(q2));
    }

    puts("\n-----------QueueMain---------");
    Queue_print(queue);

    Queue_free(q1);
    Queue_free(q2);
    Queue_free(queue);

    List_free(list);
    //
    return 0;
}
//
void List_testing()
{
    List *list;
    list = List_alloc();
    // assert(list->capacity == 4);

    List_add(list, 12.2);
    List_add(list, 145.9);
    List_add(list, 56.0);
    List_add(list, 7);
    List_add(list, 0);
    List_add(list, 17.8);
    List_add(list, 0.5);
    List_add(list, 17.8);

    assert(List_contains(list, 12.2) == true);
    assert(List_contains(list, 7) == true);
    assert(List_contains(list, 7.0) == true);
    assert(List_contains(list, 12.8) == false);
    assert(List_contains(list, 0.5) == true);

    assert(List_get(list, 0) > 12.19 || List_get(list, 0) < 12.21);
    assert(List_get(list, 1) > 6.99 || List_get(list, 1) < 7.01);
    assert(List_get(list, 2) > 6.99 || List_get(list, 2) < 7.01);

    assert(List_indexOf(list, 12.2) == 0);
    assert(List_indexOf(list, 145.9) == 1);
    assert(List_indexOf(list, 0) == 4);

    List_insert(list, 5, 95.369);
    assert(List_contains(list, 95.369) == true);
    // assert(List_size(list) == 8);
    assert(List_get(list, 6) > 17.79 || List_get(list, 6) < 17.81);

    List_print(list);
    List_removeAt(list, 5);
    assert(List_get(list, 5) > 17.79 || List_get(list, 6) < 17.81);
    List_remove(list, 0);
    List_remove(list, 17.8);
    List_print(list);

    assert(List_isEmpty(list) == false);
    List_clear(list);
    assert(List_isEmpty(list) == true);

    List_free(list);
    puts("---------------------list verification successful---------------- ");
}
//
void Queue_testing()
{
    Queue *q;
    q = Queue_alloc();

    assert(Queue_isEmpty(q) == true);
    Queue_enqueue(q, 48.5);
    assert(Queue_size(q) == 1);

    Queue_enqueue(q, 5.5);
    assert(Queue_size(q) == 2);

    Queue_enqueue(q, 0.5);
    assert(Queue_size(q) == 3);

    Queue_enqueue(q, -25);
    assert(Queue_size(q) == 4);

    Queue_enqueue(q, 11.25);
    Queue_enqueue(q, 12.98633);
    Queue_enqueue(q, 13.4785);
    Queue_enqueue(q, 14.12);
    Queue_enqueue(q, 15);
    Queue_enqueue(q, 16.09);
    Queue_enqueue(q, 17.000);
    Queue_enqueue(q, -185);
    Queue_enqueue(q, 1966.4);

    assert(Queue_size(q) == 13);
    int length = Queue_size(q);
    float del[length];

    for (int i = 0; i < length; i++)
    {
        del[i] = Queue_dequeue(q);
    }
    Queue_dequeue(q);

    assert(Queue_size(q) == 0);
    assert(Queue_isEmpty(q) == true);

    Queue_print(q);
    Queue_free(q);
}
//
void List_print(List *self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        printf(" %.3f ", List_get(self, i));
    }
    puts(" ");
}
//
void Queue_print(Queue *self)
{
    for (int i = self->first; i < self->last; i++)
    {
        printf(" ---- %.3f ----- \n", self->items[i]);
    }
}
