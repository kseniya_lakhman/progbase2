#include "queue.h"
//
void Queue_init(Queue *self)
{
    self->capacity = 4;
    self->first = 0;
    self->last = 0;
    self->items = malloc(sizeof(self->items) * self->capacity);

    if (self->items == NULL)
    {
        fprintf(stderr, "Allocating error");
        abort();
    }
}
//
void Queue_deinit(Queue *self)
{
    free(self->items);
}
//
Queue *Queue_alloc(void)
{
    Queue *self = malloc(sizeof(Queue));
    Queue_init(self);
    return self;
}
//
void Queue_free(Queue *self)
{
    Queue_deinit(self);
    free(self);
}
//
static void Queue_realloc(Queue *self, int newCapacity)
{
    float *newItems = realloc(self->items, sizeof(self->items[0]) * newCapacity);
    if (newItems == NULL)
    {
        fprintf(stderr, "Reallocation error");
        abort();
    }
    self->items = newItems;
    self->capacity = newCapacity;
    // printf("Reallocated\n");
}
//
void Queue_enqueue(Queue *self, float value)
{
    self->items[self->last] = value;
    self->last += 1;

    if (self->last == self->capacity)
    {
        int newCap = self->capacity * 2;
        Queue_realloc(self, newCap);
    }

    if (self->last == self->first)
    {
        int newCap = self->capacity * 2;
        Queue_realloc(self, newCap);
    }
}
//
float Queue_dequeue(Queue *self)
{
    float value = self->items[self->first];
    self->first += 1;
    if (self->first == self->capacity)
    {
        self->first = 0;
    }
    return value;
}
//
size_t Queue_size(Queue *self)
{
    if (self->last >= self->first)
    {
        return self->last - self->first;
    }
    fprintf(stderr, "Queue is empty\n");
    return 0;
}
//
bool Queue_isEmpty(Queue *self)
{
    return Queue_size(self) == 0;
}
