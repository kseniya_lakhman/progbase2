#include "queue.h"
#include "list.h"
#include <progbase/console.h>
#include <cmath>
#include <iostream>
#include <string>
#include <cassert>

void List_print(List &self);
void Queue_print(Queue &self);
void recordFromFile(List &list, FILE *fin);
void processList(List &list);
void processQueue(List &list, Queue &queue, Queue &q1, Queue &q2);

void List_testing();
void Queue_testing();

int main()
{
    //
    List_testing();
    Queue_testing();
    Console_clear();
    //

    List list;
    FILE *fin = fopen("data.txt", "r");

    puts(" >>>----------------------List------------------------------------------\n");
    recordFromFile(list, fin);

    puts("\n--------------->> List from file----------------------------------------");
    List_print(list);

    processList(list);
    puts("\n--------------->> Numbers greater than 10 moved to the top of list------");
    List_print(list);

    Queue q1;
    Queue q2;
    Queue queue;

    puts("\n >>>--------------------Queue-------------------------------------------\n");

    processQueue(list, queue, q1, q2);

    puts("\n-----------QueueMain----------------------------------------------------");
    Queue_print(queue);

    return 0;
}
//
void List_testing()
{
    List list;

    list.add(12.2);
    list.add(145.9);
    list.add(56.0);
    list.add(7);
    list.add(0);
    list.add(17.8);
    list.add(0.5);
    list.add(17.8);

    assert(list.contains(12.2) == true);
    assert(list.contains(7) == true);
    assert(list.contains(7.0) == true);
    assert(list.contains(12.8) == false);
    assert(list.contains(0.5) == true);

    assert(list[0] > 12.19 || list[0] < 12.21);
    assert(list[1] > 6.99 || list[1] < 7.01);
    assert(list[2] > 6.99 || list[2] < 7.01);

    assert(list.indexOf(12.2) == 0);
    assert(list.indexOf(145.9) == 1);
    assert(list.indexOf(0) == 4);

    list.insert(5, 95.369);
    assert(list.contains(95.369) == true);
    assert(list[6] > 17.79 || list[6] < 17.81);

    List_print(list);
    list.removeAt(5);

    assert(list[5] > 17.79 || list[6] < 17.81);
    list.remove(0);
    list.remove(17.8);
    List_print(list);

    assert(list.isEmpty() == false);
    list.clear();
    assert(list.isEmpty() == true);
    puts("---------------------list verification successful---------------- ");
}
//
void Queue_testing()
{
    Queue q;

    assert(q.isEmpty() == true);
    q.enqueue(48.5);
    assert(q.size() == 1);

    q.enqueue(5.5);
    assert(q.size() == 2);

    q.enqueue(0.5);
    assert(q.size() == 3);

    q.enqueue(-25);
    assert(q.size() == 4);

    q.enqueue(11.25);
    q.enqueue(12.98633);
    q.enqueue(13.4785);
    q.enqueue(14.12);
    q.enqueue(15);
    q.enqueue(16.09);
    q.enqueue(17.000);
    q.enqueue(-185);
    q.enqueue(1966.4);

    assert(q.size() == 13);
    int length = static_cast<signed int>(q.size());
    float del[13];

    for (int i = 0; i < length; i++)
    {
        del[i] = q.dequeue();
    }
    q.dequeue();

    assert(q.size() == 0);
    assert(q.isEmpty() == true);

    Queue_print(q);
}
//
void List_print(List &self)
{
    for (int i = 0; i < static_cast<signed int>(self.size()); i++)
    {
        std::cout << self[i] << " | ";
    }
    std::cout << std::endl;
}
// //
void Queue_print(Queue &self)
{
    for (int i = self.first(); i < static_cast<signed int>(self.last()); i++)
    {
        std::cout << "----  " << self.get(i) << std::endl;
    }
    std::cout << std::endl;
}

void recordFromFile(List &list, FILE *fin)
{
    if (fin == nullptr)
    {
        std::cerr << "Allocation error\n"
                  << std::endl;
        abort();
    }

    char ch = fgetc(fin);

    char buf[100];
    int buf_i = 0;

    while (1)
    {
        if (ch == ' ' || ch == EOF)
        {
            buf[buf_i] = '\0';
            buf_i = 0;
            float curr_num = atof(buf);
            if (buf[0] != '\0')
            {
                list.add(curr_num);
            }
        }
        else
        {
            buf[buf_i++] = ch;
        }
        if (ch == EOF)
        {
            break;
        }
        ch = fgetc(fin);
    }
    if (fin != NULL)
    {
        fclose(fin);
    }
}

void processList(List &list)
{
    float num = 0;
    int firts_position = 0;
    int length = static_cast<signed int>(list.size());
    for (int i = 0; i < length; i++)
    {
        num = list[i];
        if (fabs(num) > 10)
        {
            list.removeAt(i);
            list.insert(firts_position, num);
        }
    }
}

void processQueue(List &list, Queue &queue, Queue &q1, Queue &q2)
{
    for (int i = 0; i < static_cast<signed int>(list.size()); i++)
    {
        if (i % 2 == 0)
        {
            q2.enqueue(list[i]);
        }
        else
        {
            q1.enqueue(list[i]);
        }
    }

    puts("------->> Queue1------");
    Queue_print(q1);
    puts("\n------->> Queue2------");
    Queue_print(q2);

    int length1 = static_cast<signed int>(q1.size());
    for (int i = 0; i < length1; i++)
    {
        queue.enqueue(q1.dequeue());
    }

    int length2 = static_cast<signed int>(q2.size());
    for (int i = 0; i < length2; i++)
    {
        queue.enqueue(q2.dequeue());
    }
}