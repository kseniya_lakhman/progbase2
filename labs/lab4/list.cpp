#include "list.h"
#include <cassert>
#include <iostream>

List::List()
{
    this->capacity_ = 4;
    this->items_ = (float *)(malloc(sizeof(this->items_[0]) * this->capacity_));
    this->size_ = 0;
    if (this->items_ == NULL)
    {
        // fprintf(stderr, "Allocation error\n");
        std::cerr << "Allocation error\n"
                  << std::endl;
        abort();
    }
}

List::~List()
{
    free(this->items_);
}

static void List_InvalidIndexError()
{
    assert(0 && "Invalid index");
    fprintf(stderr, "Invalid index");
}
//
void List::reallocation(size_t newCapacity)
{
    float *newArray = static_cast<float *>(std::realloc(this->items_, sizeof(this->items_[0]) * newCapacity));
    if (newArray == NULL)
    {
        fprintf(stderr, "Reallocation error\n");
        abort();
    }
    this->items_ = newArray;
    this->capacity_ = newCapacity;
}
size_t List::size()
{
    return this->size_;
}
//
void List::insert(int index, float value)
{
    if (index < 0 || index > static_cast<signed int>(List::size()))
    {
        List_InvalidIndexError();
        return;
    }

    if (this->size_ == this->capacity_)
    {
        int newCapacity = this->capacity_ * 2;
        List::reallocation(newCapacity);
    }

    for (int i = this->size_ - 1; i >= index; i--)
    {
        this->items_[i + 1] = this->items_[i];
    }
    this->items_[index] = value;
    this->size_ += 1;
}
//
void List::removeAt(int index)
{
    if (index < 0 || index > static_cast<signed int>(List::size()))
    {
        List_InvalidIndexError();
        return;
    }

    for (int i = index; i < static_cast<signed int>(List::size()); i++)
    {
        this->items_[i] = this->items_[i + 1];
    }
    this->size_ -= 1;
}
//
void List::add(float value)
{
    this->items_[this->size_] = value;
    this->size_ += 1;
    if (this->size_ == this->capacity_)
    {
        int newCapacity = this->capacity_ * 2;
        reallocation(newCapacity);
    }
}
//
void List::remove(float value)
{
    List::removeAt(List::indexOf(value));
}
//
int List::indexOf(float value)
{

    for (int i = 0; i < static_cast<signed int>(this->size()); i++)
    {
        if (this->items_[i] == value)
        {
            return i;
        }
    }
    return -1;
}
//
bool List::contains(float value)
{
    return this->indexOf(value) != -1;
}

bool List::isEmpty()
{
    return (this->size()) == 0;
}
//
void List::clear()
{
    this->size_ = 0;
}
//
float &List::operator[](int index)
{
    int length = static_cast<signed int>(List::size());

    if (index < 0 || index > length)
    {
        List_InvalidIndexError();
        abort();
    }
    return this->items_[index];
}