#pragma once
//

#include <cstdio>
#include <cstdlib>

class Queue
{
  float *items_;
  int capacity_;
  int first_;
  int last_;

private:
  void reallocation(int newCapacity);

public:
  Queue();
  ~Queue();

  size_t size();

  void enqueue(float value);
  float dequeue();

  float first();
  float last();

  float get(int index);
  // float &operator[](int index);

  bool isEmpty();
};