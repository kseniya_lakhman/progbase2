#include "queue.h"
//
Queue::Queue()
{
    this->capacity_ = 4;
    this->first_ = 0;
    this->last_ = 0;
    this->items_ = static_cast<float *>(malloc(sizeof(this->items_) * this->capacity_));

    if (this->items_ == NULL)
    {
        fprintf(stderr, "Allocating error");
        abort();
    }
}
//
Queue::~Queue()
{
    free(this->items_);
}
//
void Queue::reallocation(int newCapacity)
{
    float *newItems = static_cast<float *>(realloc(this->items_, sizeof(this->items_[0]) * newCapacity));
    if (newItems == NULL)
    {
        fprintf(stderr, "Reallocation error");
        abort();
    }
    this->items_ = newItems;
    this->capacity_ = newCapacity;
}
//
void Queue::enqueue(float value)
{
    this->items_[this->last_] = value;
    this->last_ += 1;

    if (this->last_ == this->capacity_)
    {
        int newCap = this->capacity_ * 2;
        Queue::reallocation(newCap);
    }

    if (this->last_ == this->first_)
    {
        int newCap = this->capacity_ * 2;
        Queue::reallocation(newCap);
    }
}
//
float Queue::dequeue()
{
    float value = this->items_[this->first_];
    this->first_ += 1;
    if (this->first_ == this->capacity_)
    {
        this->first_ = 0;
    }
    return value;
}
//
size_t Queue::size()
{
    if (this->last_ >= this->first_)
    {
        return this->last_ - this->first_;
    }
    fprintf(stderr, "Queue is empty\n");
    return 0;
}
//
bool Queue::isEmpty()
{
    return Queue::size() == 0;
}

float Queue::first()
{
    return this->first_;
}

float Queue::last()
{
    return this->last_;
}

float Queue::get(int index)
{
    int length = static_cast<signed int>(Queue::size());
    if (index < 0 || index > length)
    {
        fprintf(stderr, "Invalid index");
        return -1;
    }
    return this->items_[index];
}

// float &Queue::operator[](int index)
// {
//     int length = static_cast<signed int>(Queue::size());

//     if (index < 0 || index > length)
//     {
//         fprintf(stderr, "Invalid index");
//         abort();
//     }
//     return this->items_[index];
// }