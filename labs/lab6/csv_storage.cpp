#include "csv_storage.h"
#include <fstream>
#include <iostream>

using namespace std;

void writeAllToFile(string const &filename, string const &str_text);
string readAllFromFile(string const &filename);

Composer CsvStorage::rowToComposer(const CsvRow &row)
{
    Composer com;

    com.id = stoi(row[0]);
    com.fullname = row[1];
    com.year = stoi(row[2]);
    com.composition = stoi(row[3]);
    com.amount = stoi(row[4]);

    return com;
}

CsvRow CsvStorage::composerToRow(const Composer &st)
{
    CsvRow row;
    row[0] = st.id;
    row[1] = st.fullname;
    row[2] = st.year;
    row[3] = st.composition;
    row[4] = st.amount;
    return row;
}

Album CsvStorage::rowToAlbum(const CsvRow &row)
{
    Album a;
    a.id = stoi(row[0]);
    a.composerName = row[1];
    a.albumName = row[2];
    a.kind = row[3];
    a.capacity = stoi(row[4]);
    return a;
}
CsvRow CsvStorage::albumToRow(const Album &cs)
{
    CsvRow row;

    row[0] = cs.id;
    row[1] = cs.composerName;
    row[2] = cs.albumName;
    row[3] = cs.kind;
    row[4] = cs.capacity;

    return row;
}

int CsvStorage::getNewComposerId()
{
    int max_id = 0;
    for (Composer &c : this->composers_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
}

bool CsvStorage::load()
{
    cout << "HERE" << endl;
    string composers_filename = this->dir_name_ + "/composers.csv";
    string composers_csv = readAllFromFile(composers_filename);
    CsvTable composers_table = Csv::createTableFromString(composers_csv);
    for (CsvRow &row : composers_table)
    {
        Composer c;
        c.id = std::stoi(row[0]);
        c.fullname = row[1];
        c.year = std::stoi(row[2]);
        c.composition = row[3];
        c.amount = std::stoi(row[4]);
        this->composers_.push_back(c);
    }

    string albums_filename = this->dir_name_ + "/albums.csv";
    string albums_csv = readAllFromFile(albums_filename);
    CsvTable albums_table = Csv::createTableFromString(albums_csv);

    for (CsvRow &rowAl : albums_table)
    {
        Album a;
        a.id = std::stoi(rowAl[0]);
        a.composerName = rowAl[1];
        a.albumName = rowAl[2];
        a.kind = rowAl[3];
        a.capacity = std::stoi(rowAl[4]);

        this->albums_.push_back(a);
    }

    return true;
}

void printfStrings(vector<string> &self)
{
    for (int i = 0; i < self.size(); i++)
    {
        cout << "| " << self[i] << " |";
    }
    puts(" ");
}

bool CsvStorage::save() // write to FILE
{
    string composers_filename = this->dir_name_ + "/composers.csv";
    CsvTable t;
    for (Composer &c : this->composers_)
    {
        CsvRow row;
        row.push_back(std::to_string(c.id));
        row.push_back(c.fullname);
        row.push_back(std::to_string(c.year));
        row.push_back(c.composition);
        row.push_back(std::to_string(c.amount));

        t.push_back(row);
    }
    string csv_text = Csv::createStringFromTable(t);
    writeAllToFile(composers_filename, csv_text);

    string albums_filename = this->dir_name_ + "/albums.csv";
    CsvTable tAl;
    for (Album &cAl : this->albums_)
    {
        CsvRow rowAl;
        rowAl.push_back(std::to_string(cAl.id));
        rowAl.push_back(cAl.composerName);
        rowAl.push_back(cAl.albumName);
        rowAl.push_back(cAl.kind);
        rowAl.push_back(std::to_string(cAl.capacity));

        tAl.push_back(rowAl);
    }
    string csv_textAl = Csv::createStringFromTable(tAl);
    writeAllToFile(albums_filename, csv_textAl);

    return true;
}

vector<Composer> CsvStorage::getAllComposers()
{
    return this->composers_;
}

optional<Composer> CsvStorage::getComposerById(int composer_id)
{
    for (Composer &c : this->composers_)
    {
        if (c.id == composer_id)
        {
            return c;
        }
    }
    return nullopt;
}

bool CsvStorage::updateComposer(const Composer &composer)
{
    for (Composer &c : this->composers_)
    {
        if (c.id == composer.id)
        {
            c.fullname = composer.fullname;
            c.composition = composer.composition;
            c.year = composer.year;
            c.amount = composer.amount;
            return true;
        }
    }
    return false;
}

bool CsvStorage::removeComposer(int composer_id)
{
    int index = -1;
    for (int i = 0; i < this->composers_.size(); i++)
    {
        if (this->composers_[i].id == composer_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->composers_.erase(this->composers_.begin() + index);
        return true;
    }
    return false;
}

int CsvStorage::insertComposer(const Composer &composer)
{
    int new_id = this->getNewComposerId();
    Composer copy = composer;
    copy.id = new_id;
    this->composers_.push_back(copy);
    return new_id;
}
// /////
string readAllFromFile(string const &filename)
{
    ifstream file;
    file.open(filename);

    if (!file.good())
    {
        cerr << "Can`t open file:" << filename << endl;
        abort();
    }
    string row_str;
    string text_str;
    while (std::getline(file, row_str))
    {
        text_str += row_str + "\n";
    }
    file.close();
    return text_str;
}

void writeAllToFile(string const &filename, string const &str_text)
{
    ofstream file;
    file.open(filename);
    if (!file.is_open())
    {
        cerr << "Error: can`t open the file for writing: " << filename;
        abort();
    }
    file << str_text;
    file.close();
}
// /////
vector<Album> CsvStorage::getAllAlbums(void)
{
    return this->albums_;
}
optional<Album> CsvStorage::getAlbumById(int album_id)
{
    for (Album &c : this->albums_)
    {
        if (c.id == album_id)
        {
            return c;
        }
    }
    return nullopt;
}
bool CsvStorage::updateAlbum(const Album &album)
{
    for (Album &c : this->albums_)
    {
        if (c.id == album.id)
        {
            c.composerName = album.composerName;
            c.albumName = album.albumName;
            c.capacity = album.capacity;
            c.kind = album.kind;
            return true;
        }
    }
    return false;
}
bool CsvStorage::removeAlbum(int album_id)
{
    int index = -1;
    for (int i = 0; i < this->albums_.size(); i++)
    {
        if (this->albums_[i].id == album_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->albums_.erase(this->albums_.begin() + index);
        return true;
    }
    return false;
}

int CsvStorage::getNewAlbumId()
{
    int max_id = 0;
    for (Album &c : this->albums_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
}

int CsvStorage::insertAlbum(const Album &album)
{
    int new_id = this->getNewAlbumId();
    Album copy = album;
    copy.id = new_id;
    this->albums_.push_back(copy);
    return new_id;
}
