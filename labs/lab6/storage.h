#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "composer.h"
#include "album.h"

using namespace std;

class Storage
{
 public:

   virtual bool load() = 0;
   virtual bool save() = 0;

   // students
   virtual vector<Composer> getAllComposers(void) = 0;
   virtual optional<Composer> getComposerById(int composer_id) = 0;
   virtual bool updateComposer(const Composer &composer) = 0;
   virtual bool removeComposer(int composer_id) = 0;
   virtual int insertComposer(const Composer & composer) = 0;

   // albums
   virtual vector<Album> getAllAlbums(void) = 0;
   virtual optional<Album> getAlbumById(int album_id) = 0;
   virtual bool updateAlbum(const Album &album) = 0;
   virtual bool removeAlbum(int album_id) = 0;
   virtual int insertAlbum(const Album & album) = 0;
};
