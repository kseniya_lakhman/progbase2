#include "csv.h"
#include "composer.h"
#include <ctype.h>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>

#include "csv_storage.h"
#include "xml_storage.h"

#include "cui.h"

using namespace std;

int main()
{
    XmlStorage xml_storage("../lab6/data/xml");
//    CsvStorage csv_storage("../lab6/data/csv");

    Storage * storage_ptr = &xml_storage;
    storage_ptr->load();

    Cui cui(storage_ptr);
    cui.show();
    return 0;
}
