#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include "composer.h"

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    void presentValuesInEditDialog();
    explicit AddDialog(Composer *composer, QWidget *parent = 0);
    ~AddDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::AddDialog *ui;
    Composer * change;
};

#endif // ADDDIALOG_H
