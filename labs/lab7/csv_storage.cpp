#include "csv_storage.h"
#include <fstream>
#include <iostream>

using namespace std;

void writeAllToFile(string const &filename, string const &str_text);
string readAllFromFile(string const &filename);

Composer CsvStorage::rowToComposer(const CsvRow &row)
{
    Composer com;

    com.id = stoi(row[0]);
    com.fullname = row[1];
    com.year = stoi(row[2]);
    com.composition = stoi(row[3]);
    com.amount = stoi(row[4]);

    return com;
}

CsvRow CsvStorage::composerToRow(const Composer &st)
{
    CsvRow row;
    row[0] = st.id;
    row[1] = st.fullname;
    row[2] = st.year;
    row[3] = st.composition;
    row[4] = st.amount;
    return row;
}
int CsvStorage::getNewComposerId()
{
    int max_id = 0;
    for (Composer &c : this->composers_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
}

bool CsvStorage::load()
{
    string composers_filename = this->dir_name_ + "/composers.csv";
    string composers_csv = readAllFromFile(composers_filename);
    CsvTable composers_table = Csv::createTableFromString(composers_csv);
    for (CsvRow &row : composers_table)
    {
        Composer c;
        c.id = std::stoi(row[0]);
        c.fullname = row[1];
        c.year = std::stoi(row[2]);
        c.composition = row[3];
        c.amount = std::stoi(row[4]);
        this->composers_.push_back(c);
    }

    return true;
}

void printfStrings(vector<string> &self)
{
    for (int i = 0; i < self.size(); i++)
    {
        cout << "| " << self[i] << " |";
    }
    puts(" ");
}

bool CsvStorage::save() // write to FILE
{
    string composers_filename = this->dir_name_ + "/composers.csv";
    CsvTable t;
    for (Composer &c : this->composers_)
    {
        CsvRow row;
        row.push_back(std::to_string(c.id));
        row.push_back(c.fullname);
        row.push_back(std::to_string(c.year));
        row.push_back(c.composition);
        row.push_back(std::to_string(c.amount));

        t.push_back(row);
    }
    string csv_text = Csv::createStringFromTable(t);
    writeAllToFile(composers_filename, csv_text);

    return true;
}

vector<Composer> CsvStorage::getAllComposers()
{
    return this->composers_;
}

optional<Composer> CsvStorage::getComposerById(int composer_id)
{
    for (Composer &c : this->composers_)
    {
        if (c.id == composer_id)
        {
            return c;
        }
    }
    return nullopt;
}

bool CsvStorage::updateComposer(const Composer &composer)
{
    for (Composer &c : this->composers_)
    {
        if (c.id == composer.id)
        {
            c.fullname = composer.fullname;
            c.composition = composer.composition;
            c.year = composer.year;
            c.amount = composer.amount;
            return true;
        }
    }
    return false;
}

bool CsvStorage::removeComposer(int composer_id)
{
    int index = -1;
    for (int i = 0; i < this->composers_.size(); i++)
    {
        if (this->composers_[i].id == composer_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->composers_.erase(this->composers_.begin() + index);
        return true;
    }
    return false;
}

int CsvStorage::insertComposer(const Composer &composer)
{
    int new_id = this->getNewComposerId();
    Composer copy = composer;
    copy.id = new_id;
    this->composers_.push_back(copy);
    return new_id;
}
// /////
string readAllFromFile(string const &filename)
{
    ifstream file;
    file.open(filename);

    if (!file.good())
    {
        cerr << "Can`t open file:" << filename << endl;
        abort();
    }
    string row_str;
    string text_str;
    while (std::getline(file, row_str))
    {
        text_str += row_str + "\n";
    }
    file.close();
    return text_str;
}

void writeAllToFile(string const &filename, string const &str_text)
{
    ofstream file;
    file.open(filename);
    if (!file.is_open())
    {
        cerr << "Error: can`t open the file for writing: " << filename;
        abort();
    }
    file << str_text;
    file.close();
}
