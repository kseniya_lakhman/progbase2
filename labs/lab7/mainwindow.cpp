#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "adddialog.h"
#include "composer.h"
#include "storage.h"
#include "xml_storage.h"

#include <QListWidgetItem>
#include <QFileDialog>
#include <QMessageBox>
#include <QFileDialog>

Q_DECLARE_METATYPE(Composer*)
Q_DECLARE_METATYPE(Composer)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->composersLabel->setText("<b>Composers:</b>");
    ui->selectedComposerLabel->setText("<b>Selected composer:</b>");

    if (this->storage == nullptr)
    {
        ui->addButton->setEnabled(false);
        ui->editButton->setEnabled(false);
        ui->removeButton->setEnabled(false);
    }

    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::beforeExit);
    connect(ui->actionNew_storage, &QAction::triggered, this, &MainWindow::saveNewStorage);
    connect(ui->actionOpen_Storage, &QAction::triggered, this, &MainWindow::openNewStorage);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.count() == 0)
    {
        qDebug() << "nothing to remove";
    }
    else
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
            this,
            "On delete",
            "Are you sure?",
            QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {

            foreach(QListWidgetItem * selectedItem, items)
            {
                int row_index = ui->listWidget->row(selectedItem);
                ui->listWidget->takeItem(row_index);
                QVariant var = selectedItem->data(Qt::UserRole);
                Composer *comp = static_cast<Composer *>(var.data());

                this->storage->removeComposer(comp->id);
                delete selectedItem;
            }

        } else {
            qDebug() << "Yes was *not* clicked";
        }
    }
    this->storage->save();
}
void MainWindow::beforeExit()
{
   QMessageBox::StandardButton reply;
   reply = QMessageBox::question(
       this,
       "On delete",
       tr("Are you sure?"),
       QMessageBox::Yes|QMessageBox::No);
   if (reply == QMessageBox::Yes) {
       qDebug() << "Yes was clicked";
       this->close();
   } else {
       qDebug() << "Yes was *not* clicked";
   }
}

void MainWindow::on_addButton_clicked() // @TODO
{
    Composer comp;
    AddDialog addDialog(&comp);
    addDialog.setWindowTitle("Add");
    int status = addDialog.exec();

    if (status == 1) {
      qDebug() << "Accepted!";
      int id = this->storage->insertComposer(comp);
      this->storage->save();
      comp.id = id;

      QVariant var;
      var.setValue(comp);

      QListWidgetItem * newComposerListItem = new QListWidgetItem();
      newComposerListItem->setText(comp.fullname.c_str());
      newComposerListItem->setData(Qt::UserRole, var);
      ui->listWidget->addItem(newComposerListItem);
     } else {
              qDebug() << "Rejected!";
     }
}

Composer copy(Composer * composer)
{
    Composer c;

    c.id = composer->id;
    c.fullname = composer->fullname;
    c.composition = composer->composition;
    c.year = composer->year;
    c.amount = composer->amount;

    return c;

}

void MainWindow::on_editButton_clicked()
{
    QList<QListWidgetItem *> item = ui->listWidget->selectedItems();
    QListWidgetItem * selectedComposer = item.at(0);
    QVariant var = selectedComposer->data(Qt::UserRole);
    Composer * comp = static_cast<Composer*>(var.data());

    AddDialog editDialog(comp);
    editDialog.setWindowTitle("Edit");
    editDialog.presentValuesInEditDialog();
    int status = editDialog.exec();

    if (status == 1)
    {
      qDebug() << "Accepted!";
      var.setValue(copy(comp));

      selectedComposer->setText(comp->fullname.c_str());
      selectedComposer->setData(Qt::UserRole, var);

      this->storage->updateComposer(copy(comp));
      this->storage->save();

    } else
    {
        qDebug() << "Rejected!";
    }
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    bool isOnlyOneItemSelectedItem = false;
    bool isSelected = false;
    if (ui->listWidget->selectedItems().size() >= 1)
    {
        isSelected = true;
        if(ui->listWidget->selectedItems().size() == 1)
        {
            isOnlyOneItemSelectedItem = true;
        }
    }

    ui->editButton->setEnabled(isOnlyOneItemSelectedItem);
    ui->removeButton->setEnabled(isSelected);
    if (item != nullptr)
    {
        QVariant var = item->data(Qt::UserRole);
        Composer *comp = static_cast<Composer*>(var.data());
        ui->fullnameTextBrowser->setText(QString::fromStdString(comp->fullname));
        ui->compositionTextBrowser->setText(QString::fromStdString(comp->composition));
        ui->yearTextBrowser->setText(QString::number(comp->year));
        ui->amountTextBrowser->setText(QString::number(comp->amount));
    }
}

void MainWindow::saveNewStorage() //save
{
       QFileDialog dialog(this);
       dialog.setFileMode(QFileDialog::Directory);
       QString current_dir = QDir::currentPath();
       QString default_name = "new_storage";

       QString folder_path = dialog.getSaveFileName(
           this,
           "Select New Storage Folder",
           current_dir + "/" + default_name,
           "Folders");
       if (folder_path != nullptr)
       {
          ui->listWidget->clear();
          XmlStorage * xml_storage = new XmlStorage(folder_path.toStdString());
          this->storage = xml_storage;
          this->storage->save();
          this->storage->load();
          ui->addButton->setEnabled(true);
       }
}

void MainWindow::setGUIFromOpenFile(vector <Composer> composers)
{
    foreach(Composer c, composers)
    {
        QVariant var;
        var.setValue(c);

        QListWidgetItem * newComposerListItem = new QListWidgetItem();
        newComposerListItem->setText(c.fullname.c_str());
        newComposerListItem->setData(Qt::UserRole, var);
        ui->listWidget->addItem(newComposerListItem);
    }
}

void MainWindow::openNewStorage()
{
   QString fileName = QFileDialog::getOpenFileName(
               this,              // parent
               "Open",  // caption
               "",                // directory to start with
               "XML (*.xml);;All Files (*)");  // file name filter
   qDebug() << fileName;
   if (fileName != nullptr)
   {
       ui->listWidget->clear();

       XmlStorage * xml_storage = new XmlStorage(fileName.toStdString());
       this->storage = xml_storage;
       this->storage->load();
       setGUIFromOpenFile(xml_storage->getAllComposers());
       ui->addButton->setEnabled(true);
   }

}
