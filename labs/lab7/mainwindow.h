#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QDebug>
#include <storage.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_removeButton_clicked();
    void on_addButton_clicked();
    void on_editButton_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void saveNewStorage();
    void openNewStorage();
    void beforeExit();

private:
    Ui::MainWindow *ui;
    Storage * storage;
    void setGUIFromOpenFile(vector <Composer> composers);
};

#endif // MAINWINDOW_H
