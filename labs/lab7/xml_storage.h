#pragma once

#include <vector>
#include <string>
#include <QtXml>
#include "storage.h"

#include "optional.h"
#include "composer.h"

using std::string;
using std::vector;

class XmlStorage : public Storage
{
  const string dir_name_;

  vector<Composer> composers_;

  int getNewComposerId();

public:
  XmlStorage(const string &dir_name) : dir_name_(dir_name) {}

  bool load();
  bool save();

  // Composers
  vector<Composer> getAllComposers(void);
  optional<Composer> getComposerById(int composer_id);
  bool updateComposer(const Composer & composer);
  bool removeComposer(int composer_id);
  int insertComposer(const Composer &composer);
};
