#include "xml_storage.h"
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QtXml>
#include <QDebug>
#include <string>

int XmlStorage::getNewComposerId()
{
    int max_id = 0;
    for (Composer &c : this->composers_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
    return 0;
}
QDomDocument readAllFromXmlFile(string const &filename)
{
    QString composers_filename = QString::fromStdString(filename);
    QFile file(composers_filename);
    bool is_composers_opened = file.open(QFile::ReadOnly);

    if (!is_composers_opened)
    {
        cerr << "Can`t open the file to read: " << filename;
        abort();
    }

    //
    QTextStream ts(&file);
    QString text = ts.readAll();

    QDomDocument doc;
    QString errorMessage;
    int errorLine;
    int errorColumn;

    bool is_passed = doc.setContent(text, &errorMessage, &errorLine, &errorColumn);
    if (!is_passed)
    {
        qDebug() << "Error parsing xml: " << errorMessage;
        qDebug() << "in line: " << errorLine;
    }
    file.close();
    return doc;
}

Composer domElementToComposer(QDomElement &element)
{
    Composer c;
    c.id = element.attributeNode("id").value().toInt();
    c.fullname = element.attributeNode("fullname").value().toStdString();
    c.year = element.attributeNode("year").value().toInt();
    c.composition = element.attributeNode("composition").value().toStdString();
    c.amount = element.attributeNode("amount").value().toInt();

    return c;
}
//
bool XmlStorage::load()
{

    std::string filenameComposer = this->dir_name_;
    QDomDocument composers_doc = readAllFromXmlFile(filenameComposer);
    QDomElement composers_root = composers_doc.documentElement();

    for (int i = 0; i < composers_root.childNodes().size(); i++)
    {
        QDomNode composers_node = composers_root.childNodes().at(i);
        if (composers_node.isElement())
        {
            QDomElement composers_element = composers_node.toElement();
            Composer composer = domElementToComposer(composers_element);
            this->composers_.push_back(composer);
        }
    }
    return true;
}

QDomElement composerToDomElement(QDomDocument &doc, Composer &c)
{

    QDomElement composer_el = doc.createElement("composer");
    composer_el.setAttribute("id", c.id);
    composer_el.setAttribute("fullname", c.fullname.c_str());
    composer_el.setAttribute("year", c.year);
    composer_el.setAttribute("composition", c.composition.c_str());
    composer_el.setAttribute("amount", c.amount);

    return composer_el;
}

QString createXmlComposers_Text(vector<Composer> &composers)
{
    QDomDocument composers_doc;
    QDomElement composers_root = composers_doc.createElement("composers");
    for (Composer &composer : composers)
    {
        QDomElement composer_el = composerToDomElement(composers_doc, composer);
        composers_root.appendChild(composer_el);
    }

    composers_doc.appendChild(composers_root);
    QString composers_xml_text = composers_doc.toString(4);
    return composers_xml_text;
}

void writeXmlTextToFile(string const &filename, QString &xml_text)
{
    QString mainFilename = QString::fromStdString(filename);

    QFile file(mainFilename);
    if (!file.open(QFile::WriteOnly))
    {
        qDebug() << "Can`t open file to write: " << mainFilename;
        abort();
    }

    QTextStream ts(&file);
    ts << xml_text;
    file.close();
}

bool XmlStorage::save()
{ //write to File
    //
    QString composers_xml_text = createXmlComposers_Text(this->composers_);
    std::string filenameComp = this->dir_name_;
    writeXmlTextToFile(filenameComp, composers_xml_text);

    return true;
}

// Composers
vector<Composer> XmlStorage::getAllComposers(void)
{
    return this->composers_;
}
optional<Composer> XmlStorage::getComposerById(int composer_id)
{
    for (Composer &c : this->composers_)
    {
        if (c.id == composer_id)
        {
            return c;
        }
    }
    return nullopt;
}
bool XmlStorage::updateComposer(const Composer &composer)
{
    qDebug() << "I NEED TO FIND" << composer.id;
    for (Composer &c : this->composers_)
    {
        qDebug() << "i`m finding: " << c.id;
        if (c.id == composer.id)
        {
            c = composer;
            return true;
        }
    }
    return false;
}
bool XmlStorage::removeComposer(int composer_id)
{
    int index = -1;
    for (int i = 0; i < this->composers_.size(); i++)
    {
        if (this->composers_[i].id == composer_id)
        {
            index = i;
            qDebug() << "Removing: Index had found: " << index;
            break;
        }
    }
    if (index >= 0)
    {
        this->composers_.erase(this->composers_.begin() + index);
        return true;
    }
    return false;
}
int XmlStorage::insertComposer(const Composer &composer)
{
    int new_id = this->getNewComposerId();
    Composer copy = composer;
    copy.id = new_id;
    this->composers_.push_back(copy);
    return new_id;
}
