#include "adddialog.h"
#include "ui_adddialog.h"
#include <QWidget>
#include <QDebug>
#include <QSpinBox>

Q_DECLARE_METATYPE(Composer*)
//Q_DECLARE_METATYPE(Composer)

AddDialog::AddDialog(Composer *composer, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
    change = composer;
    ui->newComposer_label->setText("<b> Add new composer: <b/>");
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_buttonBox_accepted()
{
    change->fullname = ui->fullname_lineEdit->text().toStdString();
    change->composition = ui->composition_LineEdit->text().toStdString();
    change->year = ui->year_spinBox->text().toInt();
    change->amount = ui->amount_LineEdit->text().toInt();

}

void AddDialog::presentValuesInEditDialog()
{
    ui->fullname_lineEdit->setText(QString::fromStdString(change->fullname));
    ui->composition_LineEdit->setText(QString::fromStdString(change->composition));
    ui->year_spinBox->setValue(change->year);
    ui->amount_LineEdit->setText(QString::number(change->amount));
}
