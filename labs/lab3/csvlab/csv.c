// #include "list.h" // generic list (array implementation)
#include "csv.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

void Csv_addInt(List *row, int value) // malloc a string and write value in it
{
    char *str = malloc(sizeof(char) * 10);
    sprintf(str, "%d", value);
    List_add(row, str);
}
void Csv_addDouble(List *row, double value) // malloc a str and write value in it
{
    char *str = malloc(sizeof(char) * 10);
    sprintf(str, "%lf", value);
    List_add(row, str);
}

void Csv_addString(List *row, const char *value) // malloc a string and copy value
{
    List_add(row, (void *)value);
}

int Csv_int(List *row, int index) // get item (str) and convert it to int
{
    return atoi(List_get(row, index));
}
double Csv_double(List *row, int index) // get str and convert it to double
{
    return atof(List_get(row, index));
}
int Csv_stringCopyToBuf(List *row, int index, char *buf) // get str & copy it to buf //Csv_string
{
    char *str = List_get(row, index);
    strcpy(buf, str);
    int length = strlen(buf);
    return length;
}

void Csv_addRow(List *table, List *row) // add row to table as an item
{
    List_add(table, row);
}
List *Csv_row(List *table, int index) // get item from table as List *
{
    return (List *)List_get(table, index);
}

void Csv_fillTableFromString(List *csvTable, const char *csvStr)
{
    const char *p = csvStr;
    char buf[500];
    int bufX = 0;
    List *rowTable = List_alloc();
    bool normal = true;
    while (1)
    {
        if (normal)
        {
            if (*p == '"')
            {
                normal = false;
            }
            else
            {
                if (*p == ',' || *p == '\n' || *p == '\0')
                {
                    buf[bufX] = '\0';
                    Csv_addString(rowTable, strOnHeap(buf));
                    if (*p == '\n')
                    {
                        Csv_addRow(csvTable, rowTable);
                        rowTable = List_alloc();
                    }
                    else if (*p == '\0')
                    {
                        Csv_addRow(csvTable, rowTable);
                    }

                    bufX = 0;
                }
                else
                {
                    buf[bufX] = *p;
                    bufX += 1;
                }
            }
        }
        else // escaped
        {
            if (*p == '"')
            {
                if (*(p + 1) == '"')
                {
                    buf[bufX] = '"';
                    bufX += 1;

                    p += 1;
                }
                else
                {
                    buf[bufX] = '\0';
                    Csv_addString(rowTable, strOnHeap(buf));
                    //
                    bufX = 0;
                    //
                    if (*(p + 1) == ',')
                    {
                        p += 1;
                        normal = true;
                    }
                    else
                    {
                        fprintf(stderr, "CSV format error: invalid char '%c' after escape\n", *(p + 1));
                        abort();
                    }
                }
            }
            else
            {
                buf[bufX] = *p;
                bufX += 1;
            }
        }

        if (*p == '\0')
        {
            break;
        }
        p += 1;
    }
}

int Csv_fillStringFromTable(List *csvTable, char *buf) // fill buf with csv
{
    buf[0] = '\0';
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);
        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *value = List_get(rowItem, j);
            strcat(buf, value);
            if (j != List_size(rowItem) - 1)
            {
                strcat(buf, ",");
            }
        }
        if (i != List_size(csvTable) - 1)
        {
            strcat(buf, "\n");
        }
    }
    return 0;
}
char *Csv_createStringFromTable(List *csvTable) // malloc string array and fill it with csv
{
    char *buf = " ";
    buf = strOnHeap(buf);
    Csv_fillStringFromTable(csvTable, buf);
    return buf;
}
void Csv_clearTable(List *csvTable) // free all table allocated memory
{
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);
        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *value = List_get(rowItem, j);
            free(value);
        }
        List_free(rowItem);
    }
}

char *strOnHeap(const char *str)
{
    char *m = malloc(strlen(str) + 1);
    strcpy(m, str);
    return m;
}

void printfStrings(List *self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        char *item = List_get(self, i);
        printf("| %s |", item);
    }
    puts(" ");
}

void printfStringsTable(List *self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        List *row = List_get(self, i);
        for (int j = 0; j < List_size(row); j++)
        {
            if (j == 0)
            {
                printf("%s", (char *)List_get(row, j));
            }
            else
            {
                printf(",");
                printf("%s", (char *)List_get(List_get(self, i), j));
            }
        }
        printf("\n");
    }
    puts(" ");
}