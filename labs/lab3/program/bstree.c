#include "bstree.h"
#include <stdio.h>
#include <assert.h>

void BSTree_init(BSTree *self)
{
    self->root = NULL;
    self->size = 0;
}
void BSTree_deinit(BSTree *self)
{
}

static void insert(BinTree *node, BinTree *newNode)
{
    assert(node != NULL);
    if (newNode->key == node->key)
    { // our BST can only store values with unique keys
        fprintf(stderr, "Key %i already exists in BST\n", newNode->key);
        abort();
    }
    else if (newNode->key < node->key)
    {
        if (node->left == NULL)
        {
            node->left = newNode; // assign as left node
        }
        else
        {
            insert(node->left, newNode); // recursive call to add newNode to left subtree
        }
    }
    else
    {
        if (node->right == NULL)
        {
            node->right = newNode; // assign as right node
        }
        else
        {
            insert(node->right, newNode); // recursive call to add newNode to right subtree
        }
    }
}

void BSTree_insert(BSTree *self, StrStrMap *value)
{
    BinTree *newNode = BinTree_alloc(value);
    if (self->root == NULL)
    {
        self->root = newNode;
        self->size += 1;
    }
    else
    {
        insert(self->root, newNode);
        self->size += 1;
    }
}

static bool lookup(BinTree *node, int key)
{
    if (node == NULL)
        return false;
    if (key < node->key)
        return lookup(node->left, key); // left subtree
    else if (key > node->key)
        return lookup(node->right, key); // right subtree
    return true;                         // key == self->key
}

bool BSTree_lookup(BSTree *self, int key)
{
    return lookup(self->root, key);
}

static StrStrMap *search(BinTree *node, int key)
{
    if (node == NULL)
        return NULL;
    if (node->key == key)
        return node->value;
    if (key < node->key)
        return search(node->left, key);
    if (key > node->key)
        return search(node->right, key);

    return NULL;
}

StrStrMap *BSTree_search(BSTree *self, int key)
{
    return search(self->root, key);
}

static BinTree *searchMin(BinTree *node)
{
    if (node == NULL)
        return NULL;
    if (node->left == NULL)
        return node;
    return searchMin(node->left);
}

static StrStrMap *delete (BinTree *node, int key, BinTree *parent);

static void modifyTreeOnDelete(BinTree *node, BinTree *parent)
{
    BinTree *replacementNode = NULL;
    if (node->left == NULL && node->right == NULL)
    {                           // case A: no children
        replacementNode = NULL; // nothing to replace with
    }
    else if (node->left == NULL || node->right == NULL)
    {                                                                     // case B: one child
        BinTree *child = (node->left != NULL) ? node->left : node->right; // get a pointer to the child
        replacementNode = child;                                          // replace with it’s only child
    }
    else                                                                    /* node->left != NULL && node->right != NULL */
    {                                                                       // case C: two children
        BinTree *minNode = searchMin(node->right);                          // find node with min value in right subtree
        StrStrMap *deletedValue = delete (node->right, minNode->key, node); // remove and free min node by key
        BinTree *newMin = BinTree_alloc(deletedValue);                      // as minNode is deleted and freed in delete()
        newMin->left = node->left;
        newMin->right = node->right;
        replacementNode = newMin; // replace with the min node removed from it’s right subtree
    }
    if (parent->left == node)
        parent->left = replacementNode; // find node’s position as parent’s child
    else
        parent->right = replacementNode;
}

static StrStrMap *delete (BinTree *node, int key, BinTree *parent)
{
    if (node == NULL)
    {
        fprintf(stderr, "`%i` not found\n", key);
        abort();
    }
    if (key < node->key)
        return delete (node->left, key, node);
    else if (key > node->key)
        return delete (node->right, key, node);
    else
    {
        modifyTreeOnDelete(node, parent);
        StrStrMap *old = node->value;
        BinTree_free(node);
        return old;
    }
}

StrStrMap *BSTree_delete(BSTree *self, int key)
{
    BinTree fakeRoot;
    fakeRoot.left = self->root;
    StrStrMap *old = delete (self->root, key, &fakeRoot);
    self->size -= 1;
    self->root = fakeRoot.left;
    return old;
}

size_t BSTree_size(BSTree *self)
{
    return self->size;
}
