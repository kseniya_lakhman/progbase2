
#include <stdlib.h>
#include <stdio.h>
#include "bintree.h"

#include <string.h>

void BinTree_init(BinTree *self, StrStrMap *value)
{
    self->key = atoi(StrStrMap_get(value, "id"));
    self->value = value;
    self->left = NULL;
    self->right = NULL;
}
void BinTree_deinit(BinTree *self)
{
}

BinTree *BinTree_alloc(StrStrMap *value)
{
    BinTree *self = malloc(sizeof(BinTree));
    BinTree_init(self, value);
    return self;
}
void BinTree_free(BinTree *self)
{
    BinTree_deinit(self);
    free(self);
}

void BinTree_clear(BinTree *node)
{
    if (node == NULL)
        return;
    BinTree_clear(node->left);
    BinTree_clear(node->right);
    BinTree_free(node);
}

static void printValueOnLevel(BinTree *node, char pos, int depth)
{
    for (int i = 0; i < depth; i++)
    {
        printf("....");
    }

    printf(KRED "%c: " RESET, pos);

    if (node == NULL)
    {
        printf(KGRN "(null)\n" RESET);
    }
    else
    {
        printf(KGRN "[%i] -" RESET, node->key);                           // node->value->fullname);
        printf(" [%s] \n" RESET, StrStrMap_get(node->value, "fullname")); // node->value->fullname);
    }
}

static void print(BinTree *node, char pos, int depth)
{
    bool isNotNull = (node != NULL) && (node->left != NULL || node->right != NULL);
    if (isNotNull)
    {
        print(node->right, 'R', depth + 1);
    }
    printValueOnLevel(node, pos, depth);
    if (isNotNull)
    {
        print(node->left, 'L', depth + 1);
    }
}

void BinTree_print(BinTree *root)
{
    print(root, '+', 0);
}
