#pragma once

#include "strstrmap.h"

#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define RESET "\x1B[0m"


typedef struct __BinTree BinTree;
struct __BinTree 
{
   int key;
   StrStrMap *value;
   BinTree * left; 
   BinTree * right;
};

void   BinTree_init     (BinTree * self, StrStrMap *value);
void   BinTree_deinit   (BinTree * self);

BinTree * BinTree_alloc (StrStrMap *value);
void   BinTree_free     (BinTree * self);

void BinTree_clear(BinTree * node);
void BinTree_print(BinTree * root);