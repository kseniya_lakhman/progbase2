#include "strstrmap.h"

void StrStrMap_init(StrStrMap *self)
{
    SortedKeyValueList_init(&self->list);
}
void StrStrMap_deinit(StrStrMap *self)
{
    SortedKeyValueList_deinit(&self->list);
}

StrStrMap *StrStrMap_alloc()
{
    StrStrMap *self;
    self = malloc(sizeof(StrStrMap));
    StrStrMap_init(self);
    return self;
}
void StrStrMap_free(StrStrMap *self)
{
    StrStrMap_deinit(self);
    free(self);
}

size_t StrStrMap_size(StrStrMap *self)
{
    return SortedKeyValueList_size(&self->list);
}

void StrStrMap_add(StrStrMap *self, const char *key, const char *value)
{
    KeyValue kv;
    kv.key = key;
    kv.value = value;
    SortedKeyValueList_add(&self->list, kv);
}
bool StrStrMap_contains(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;
    return SortedKeyValueList_contains(&self->list, kv);
}
const char *StrStrMap_get(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;

    int index = SortedKeyValueList_indexOf(&self->list, kv);

    if (index == -1)
    {
        fprintf(stderr, "Item does not exist\n");
        abort();
    }
    KeyValue kvf = SortedKeyValueList_get(&self->list, index);
    return kvf.value;
}
const char *StrStrMap_set(StrStrMap *self, const char *key, const char *value)
{
    KeyValue kv;
    kv.key = key;
    kv.value = value;
    int index = SortedKeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Item does not exist\n");
        abort();
    }
    KeyValue kvOld = SortedKeyValueList_get(&self->list, index);
    SortedKeyValueList_set(&self->list, index, kv);
    return kvOld.value;
}
const char *StrStrMap_remove(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;

    int index = SortedKeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Item does not exist\n");
        abort();
    }
    KeyValue kvf = SortedKeyValueList_get(&self->list, index);
    SortedKeyValueList_removeAt(&self->list, index);
    return kvf.value;
}
void StrStrMap_clear(StrStrMap *self)
{
    SortedKeyValueList_clear(&self->list);
}

StrStrMap *createComposerMap(int id, const char *fullname, int year, const char *composition, int amount)
{
    char *idValue = String_allocFromInt(id);
    char *fullnameValue = String_allocCopy(fullname);
    char *yearValue = String_allocFromInt(year);
    char *compositionValue = String_allocCopy(composition);
    char *amountValue = String_allocFromInt(amount);

    StrStrMap *map;

    map = StrStrMap_alloc();

    StrStrMap_add(map, "id", idValue);
    StrStrMap_add(map, "fullname", fullnameValue);
    StrStrMap_add(map, "year", yearValue);
    StrStrMap_add(map, "composition", compositionValue);
    StrStrMap_add(map, "amount", amountValue);
    return map;
}

char *String_allocCopy(const char *value)
{
    char *str = malloc(sizeof(char) * 100);
    strcpy(str, value);
    return str;
}

char *String_allocFromInt(int value)
{
    char str[20];
    sprintf(str, "%d", value);

    char *num = malloc(sizeof(char) * 10);
    strcpy(num, str);
    return num;
}

void clearListOfMaps(List *composers)
{
    int length = List_size(composers);
    for (int i = 0; i < length; i++)
    {
        StrStrMap *row = List_get(composers, i);
        free((char *)StrStrMap_get(row, "id"));
        free((char *)StrStrMap_get(row, "fullname"));
        free((char *)StrStrMap_get(row, "year"));
        free((char *)StrStrMap_get(row, "composition"));
        free((char *)StrStrMap_get(row, "amount"));
        StrStrMap_free(row);
    }
}

void fillTableFromListOfMaps(List *outCsvTable, List *composers)
{
    for (int i = 0; i < List_size(composers); i++)
    {

        StrStrMap *pSt = List_get(composers, i);

        const char *id = StrStrMap_get(pSt, "id");
        const char *fullname = StrStrMap_get(pSt, "fullname");
        const char *year = StrStrMap_get(pSt, "year");
        const char *composition = StrStrMap_get(pSt, "composition");
        const char *amount = StrStrMap_get(pSt, "amount");

        List *pRow = List_alloc();

        Csv_addString(pRow, id);
        Csv_addString(pRow, strOnHeap(fullname));
        Csv_addString(pRow, year);
        Csv_addString(pRow, strOnHeap(composition));
        Csv_addString(pRow, amount);

        Csv_addRow(outCsvTable, pRow);
    }
}

void fillListOfMapsFromTable(List *inCsvTable, List *composers)
{
    for (int i = 0; i < List_size(inCsvTable); i++)
    {
        List *row = List_get(inCsvTable, i);

        char fullname[100];
        char composition[100];

        int id = Csv_int(row, 0);
        int lengthName = Csv_stringCopyToBuf(row, 1, fullname);
        int year = Csv_int(row, 2);
        int lengthComp = Csv_stringCopyToBuf(row, 3, composition);
        int amount = Csv_int(row, 4);

        StrStrMap *com;
        com = createComposerMap(id, fullname, year, composition, amount);

        List_add(composers, com);
    }
}