#include <csv.h>
#include <list.h>

#define KRED "\x1B[31m"
#define RESET "\x1B[0m"

#include "strstrmap.h"
#include "bstree.h"
#include <ctype.h>

void invalidArgumentError(char *argv);
int argNreader(char *argv[], int i);
void argOreader(char *argv[], int i, char *output);
void noArgumentsError(char *argv);
void readFileToBuf(FILE *fp, char *buf);

void recordCsvToOutputFile(List *csvTable, FILE *file);

void processItems(List *items, int N);
void processTreeItems(BSTree *composersTree, BinTree *node, int N);

void printListOfMaps(List *self);

void clearOutCsvTable(List *outCsvTable);

int main(int argc, char *argv[])
{

    StrStrMap *test_item[9];

    test_item[0] = createComposerMap(3, "Johann Bach", 1721, "Brandenburg Concertos", 1126);
    test_item[1] = createComposerMap(2, "Wolfgang Mozart", 1783, "Turkish stop", 498);
    test_item[2] = createComposerMap(4, "Ludwig van Beethoven", 1801, "Moonlight Sonata", 122);
    test_item[3] = createComposerMap(17, "Richard Wagner", 1856, "Ride of the Valkyries", 56);
    test_item[4] = createComposerMap(8, "Jonny Greenwood", 1957, "There Will Be Blood", 1456);
    test_item[5] = createComposerMap(-4, "Pyotr Tchaikovsky", 1840, "1812 Overture", 785);
    test_item[6] = createComposerMap(5, "Marco Beltrami", 1882, "The Hurt Locker", 405);
    test_item[7] = createComposerMap(14, "Alexandre Desplat", 1956, "Zero Dark Thirty", 506);
    test_item[8] = createComposerMap(-12, "Javier Navarrete", 1989, "Pan’s Labyrinth", 314);

    int testListlength = sizeof(test_item) / sizeof(test_item[0]);

    char output[100];
    char input[] = "data.csv";

    bool wasInputFile = false;
    bool wasArgAfterN = false;
    bool wasArgAfterO = false;
    bool wasBSTree = false;

    int N = 0;

    //reading arguments from console
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "data.csv") == 0)
        {
            wasInputFile = true;
        }
        else if (argv[i][0] == '-')
        {
            {
                int j = 1;
                switch (argv[i][j])
                {
                    //--
                case 'n':
                {
                    if (i != argc - 1)
                    {
                        i++;
                    }
                    else
                    {
                        noArgumentsError(argv[i]);
                    }
                    N = argNreader(argv, i);
                    wasArgAfterN = true;
                    break;
                }
                case 'o':
                {
                    if (i != argc - 1)
                        i++;
                    else
                    {
                        noArgumentsError(argv[i]);
                    }
                    argOreader(argv, i, output);

                    wasArgAfterO = true;
                    break;
                }
                case 'b':
                {
                    wasBSTree = true;
                    break;
                }
                default:
                {
                    invalidArgumentError(argv[i]);
                    break;
                }
                    //--
                }
            }
        }
    }
    //

    if (wasArgAfterO == false)
    {
        strcpy(output, "output.txt");
    }

    List inCsvTable;
    List_init(&inCsvTable);

    List composers;
    List_init(&composers);

    BSTree composersTree;
    BSTree_init(&composersTree);

    if (wasInputFile)
    {
        FILE *fp = fopen(input, "r");

        if (fp == NULL)
        {
            printf("\n---------File == NULL. Aborted-----\n");
            fclose(fp);
            List_deinit(&inCsvTable);
            List_deinit(&composers);
            abort();
        }

        char buf[500];
        readFileToBuf(fp, buf);

        if (fp != NULL)
        {
            fclose(fp);
        }

        Csv_fillTableFromString(&inCsvTable, buf);
        puts(KRED "\n|------------------inCsvTable [after reading from file]-----------------------------|\n" RESET);
        printfStringsTable(&inCsvTable);

        fillListOfMapsFromTable(&inCsvTable, &composers);
    }
    else
    {
        for (int i = 0; i < testListlength; i++)
        {
            List_add(&composers, test_item[i]);
        }
    }

    if (wasBSTree)
    {
        for (int i = 0; i < List_size(&composers); i++)
        {
            StrStrMap *value = List_get(&composers, i);
            BSTree_insert(&composersTree, value);
        }

        puts(KRED "\n|--------------------composers BSTree-----------------------------------------------|\n" RESET);
        BinTree_print(composersTree.root);

        if (wasArgAfterN)
        {
            processTreeItems(&composersTree, composersTree.root, N);
        }
        puts(KRED "\n|--------------------composers BSTree [after deleting elements]---------------------|\n" RESET);
        BinTree_print(composersTree.root);
    }

    if (wasArgAfterN)
    {
        puts(KRED "\n|--------------------composers [before deleting elements]---------------------------|\n" RESET);
        printListOfMaps(&composers);

        processItems(&composers, N);

        puts(KRED "\n|--------------------composers [after deleting elements]----------------------------|\n" RESET);
        printListOfMaps(&composers);
    }

    List outCsvTable;
    List_init(&outCsvTable);

    fillTableFromListOfMaps(&outCsvTable, &composers);
    puts(KRED "\n|--------------------outCsvTable [final result]-------------------------------------|\n" RESET);
    printfStringsTable(&outCsvTable);

    if (wasArgAfterO)
    {
        FILE *fl;
        fl = fopen(output, "w");

        if (fl != NULL)
        {
            recordCsvToOutputFile(&outCsvTable, fl);

            fclose(fl);
        }
    }

    puts(KRED "|------------------------------------------------------------------------------------|\n" RESET);

    //MEMORY FREE
    if (wasInputFile)
    {
        for (int i = 0; i < testListlength; i++)
        {
            free((char *)StrStrMap_get(test_item[i], "id"));
            free((char *)StrStrMap_get(test_item[i], "fullname"));
            free((char *)StrStrMap_get(test_item[i], "year"));
            free((char *)StrStrMap_get(test_item[i], "composition"));
            free((char *)StrStrMap_get(test_item[i], "amount"));
            StrStrMap_free(test_item[i]);
        }
    }

    BinTree_clear(composersTree.root);
    BSTree_deinit(&composersTree);

    clearListOfMaps(&composers);
    List_deinit(&composers);

    Csv_clearTable(&inCsvTable);
    List_deinit(&inCsvTable);

    clearOutCsvTable(&outCsvTable);
    List_deinit(&outCsvTable);

    //
    return 0;
}

// // ------------------------------I solemnly swear that I'm up to no good------------------------------------------------------

void invalidArgumentError(char *argv)
{

    fprintf(stderr, "Error: invalid argument  '%s' ", argv);
    puts(" ");
    abort();
}

void noArgumentsError(char *argv)
{
    fprintf(stderr, "Error: no arguments after %s", argv);
    puts(" ");
    abort();
}

int argNreader(char *argv[], int i)
{
    int N = 0;
    char buf[100];
    int bufX = 0;

    //

    for (int j = 0; j < strlen(argv[i]); j++)
    {
        if (isdigit(argv[i][j]))
        {
            buf[bufX] = argv[i][j];
            bufX++;
        }
        else
        {
            if (argv[i][j] == '-')
            {
                noArgumentsError(argv[i - 1]);
            }
            else
                invalidArgumentError(argv[i]);
        }
    }
    buf[bufX] = '\0';
    N = atoi(buf);
    bufX = 0;
    //

    return N;
}

void argOreader(char *argv[], int i, char *output)
{
    char bufO[100];
    int bufx = 0;
    for (int k = 0; k < strlen(argv[i]); k++)
    {
        bufO[bufx] = argv[i][k];
        bufx++;
    }
    bufO[bufx] = '\0';
    bufx = 0;
    strcpy(output, bufO);
}

void processItems(List *composers, int N)
{
    for (int i = 0; i < List_size(composers); i++)
    {
        StrStrMap *com = List_get(composers, i);
        int amount = atoi(StrStrMap_get(com, "amount"));

        if (amount < N)
        {
            List_removeAt(composers, i);
            {
                free((char *)StrStrMap_get(com, "id"));
                free((char *)StrStrMap_get(com, "fullname"));
                free((char *)StrStrMap_get(com, "year"));
                free((char *)StrStrMap_get(com, "composition"));
                free((char *)StrStrMap_get(com, "amount"));

                StrStrMap_free(com);
            }
            i -= 1;
        }
    }
}

void processTreeItems(BSTree *composersTree, BinTree *node, int N)
{

    if (node == NULL)
        return;
    processTreeItems(composersTree, node->left, N);
    processTreeItems(composersTree, node->right, N);

    StrStrMap *map = BSTree_search(composersTree, node->key);
    int amount = atoi(StrStrMap_get(map, "amount"));
    if (amount < N)
    {
        BSTree_delete(composersTree, node->key);
    }
}

void readFileToBuf(FILE *fp, char *buf)
{
    size_t buf_i = 0;

    if (fp != NULL)
    {

        char ch = fgetc(fp);
        while (1)
        {
            if (ch == EOF)
            {
                break;
            }

            buf[buf_i] = ch;
            ch = fgetc(fp);
            buf_i++;
        }

        buf[buf_i] = '\0';
        if (fp != NULL)
        {
            return;
        }
    }
}

void recordCsvToOutputFile(List *csvTable, FILE *file)
{
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *row = List_get(csvTable, i);
        for (int j = 0; j < List_size(row); j++)
        {
            bool wasSemInString = false;
            const char *str = List_get(row, j);
            while (*str != '\0')
            {
                if (*str == '\"' || *str == ',' || *str == '\n')
                {
                    wasSemInString = true;
                }
                str++;
            }
            if (i == 0 && j == 0)
            {
                if (wasSemInString)
                {
                    str = List_get(row, j);
                    fprintf(file, "%c", '\"');
                    while (*str != '\0')
                    {
                        if (*str == '\"')
                        {
                            fprintf(file, "%c", *str);
                            fprintf(file, "%c", '\"');
                        }
                        else
                        {
                            fprintf(file, "%c", *str);
                        }
                        str++;
                    }
                    fprintf(file, "%c", '\"');
                }
                else
                {
                    fprintf(file, "%s", (char *)List_get(row, j));
                }
            }
            else if (i != 0 && j == 0)
            {
                if (wasSemInString)
                {
                    str = List_get(row, j);
                    fprintf(file, "%c", '\"');
                    while (*str != '\0')
                    {
                        if (*str == '\"')
                        {
                            fprintf(file, "%c", *str);
                            fprintf(file, "%c", '\"');
                        }
                        else
                        {
                            fprintf(file, "%c", *str);
                        }
                        str++;
                    }
                    fprintf(file, "%c", '\"');
                }
                else
                {
                    fprintf(file, "%s", (char *)List_get(row, j));
                }
            }
            else
            {
                fprintf(file, ",");
                if (wasSemInString)
                {
                    str = List_get(row, j);
                    fprintf(file, "%c", '\"');
                    while (*str != '\0')
                    {
                        if (*str == '\"')
                        {
                            fprintf(file, "%c", *str);
                            fprintf(file, "%c", '\"');
                        }
                        else
                        {
                            fprintf(file, "%c", *str);
                        }
                        str++;
                    }
                    fprintf(file, "%c", '\"');
                }
                else
                {
                    fprintf(file, "%s", (char *)List_get(row, j));
                }
            }
        }
        if (i != List_size(csvTable) - 1)
        {
            fprintf(file, "\n");
        }
    }
}

void clearOutCsvTable(List *outCsvTable)
{
    for (int i = 0; i < List_size(outCsvTable); i++)
    {
        List *rowItem = List_get(outCsvTable, i);
        char *value = List_get(rowItem, 1);
        free(value);
        char *value1 = List_get(rowItem, 3);
        free(value1);
        List_free(rowItem);
    }
}


void printListOfMaps(List *self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        StrStrMap *pSt = List_get(self, i);

        const char *id = StrStrMap_get(pSt, "id");
        const char *fullname = StrStrMap_get(pSt, "fullname");
        const char *year = StrStrMap_get(pSt, "year");
        const char *composition = StrStrMap_get(pSt, "composition");
        const char *amount = StrStrMap_get(pSt, "amount");

        printf("%s,", id);
        printf("%s,", fullname);
        printf("%s,", year);
        printf("%s,", composition);
        printf("%s\n", amount);
    }
}