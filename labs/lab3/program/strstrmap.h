#pragma once

#include "sortedkeyvaluelist.h"

#include <csv.h>
#include <list.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>

typedef struct __StrStrMap StrStrMap;
struct __StrStrMap
{
    SortedKeyValueList list;
};

StrStrMap *StrStrMap_alloc();
void StrStrMap_free(StrStrMap *self);


void StrStrMap_init(StrStrMap *self);
void StrStrMap_deinit(StrStrMap *self);

size_t StrStrMap_size(StrStrMap *self);

void StrStrMap_add(StrStrMap *self, const char *key, const char *value);
bool StrStrMap_contains(StrStrMap *self, const char *key);
const char *StrStrMap_get(StrStrMap *self, const char *key);
const char *StrStrMap_set(StrStrMap *self, const char *key, const char *value);
const char *StrStrMap_remove(StrStrMap *self, const char *key);
void StrStrMap_clear(StrStrMap *self);

char *String_allocCopy(const char *value);
char *String_allocFromInt(int value);

StrStrMap *createComposerMap(int id, const char *fullname, int year, const char *composition, int amount);

void fillListOfMapsFromTable(List *inCsvTable, List *composers);
void fillTableFromListOfMaps(List *outCsvTable, List *composers);

void clearListOfMaps(List *composers);