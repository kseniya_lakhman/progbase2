#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "sortedkeyvaluelist.h"

static void SortedKeyValueList_InvalidIndexError()
{
    assert(0 && "Invalid index");
    fprintf(stderr, "Invalid index");
}

static void SortedKeyValueList_realloc(SortedKeyValueList *self, size_t newCapacity)
{
    void *newArray = realloc(self->items, sizeof(self->items[0]) * newCapacity);
    if (newArray == NULL)
    {
        fprintf(stderr, "Reallocation error\n");
        abort();
    }
    self->items = newArray;
    self->capacity = newCapacity;
    printf("Reallocated\n");
}

void SortedKeyValueList_init(SortedKeyValueList *self)
{
    self->capacity = 16;
    self->size = 0;
    self->items = malloc(sizeof(KeyValue) * self->capacity);
    if (self->items == NULL)
    {
        fprintf(stderr, "Allocation error\n");
        abort();
    }
}
void SortedKeyValueList_deinit(SortedKeyValueList *self)
{
    free(self->items);
}

size_t SortedKeyValueList_size(SortedKeyValueList *self)
{
    return self->size;
}
KeyValue SortedKeyValueList_get(SortedKeyValueList *self, int index)
{
    if (index < 0 || index > SortedKeyValueList_size(self))
    {
        SortedKeyValueList_InvalidIndexError();
        abort();
    }
    return self->items[index];
}

void SortedKeyValueList_set(SortedKeyValueList *self, int index, KeyValue value)
{
    if (index < 0 || index > SortedKeyValueList_size(self))
    {
        SortedKeyValueList_InvalidIndexError();
        abort();
    }
    self->items[index] = value;
}

void SortedKeyValueList_removeAt(SortedKeyValueList *self, int index)
{
    if (index < 0 || index > SortedKeyValueList_size(self))
    {
        SortedKeyValueList_InvalidIndexError();
        abort();
    }

    for (int i = index; i < self->size - 1; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    self->size -= 1;
}

int KeyValue_compare(const KeyValue *a, const KeyValue *b)
{
    return strcmp(a->key, b->key);
}

void SortedKeyValueList_add(SortedKeyValueList *self, KeyValue kv)
{
    if (self->size == self->capacity)
    {
        int newCap = self->capacity * 2;
        SortedKeyValueList_realloc(self, newCap);
    }

    for (int i = self->size; i >= 0; i--)
    {
        if (i == 0)
        {
            // stop
            self->items[i] = kv;
        }
        else
        {
            int cmp = KeyValue_compare(&kv, &self->items[i - 1]);
            if (cmp >= 0)
            {
                // stop
                self->items[i] = kv;
                break;
            }
            else
            {
                self->items[i] = self->items[i - 1];
            }
        }
    }
    self->size += 1;
}
void SortedKeyValueList_remove(SortedKeyValueList *self, KeyValue kv)
{
    int index = SortedKeyValueList_indexOf(self, kv);
    if (index == -1)
    {
        return;
    }

    for (int i = index; i < self->size - 1; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    self->size -= 1;
}

int SortedKeyValueList_indexOf(SortedKeyValueList *self, KeyValue kv)
{
    //self->items
    KeyValue *item = bsearch(&kv, self->items, self->size, sizeof(KeyValue), (__compar_fn_t)KeyValue_compare);
    if (item == NULL)
    {
        return -1;
    }
    return item - self->items;
}
bool SortedKeyValueList_contains(SortedKeyValueList *self, KeyValue kv)
{
    return SortedKeyValueList_indexOf(self, kv) >= 0;
}

void SortedKeyValueList_clear(SortedKeyValueList *self)
{
    self->size = 0;
    SortedKeyValueList_deinit(self);
}