#include "sqlite_storage.h"
#include <QtSql>

SqliteStorage::SqliteStorage(const string &dir_name) : dir_name_(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_) + "/data.sqlite";
    db_.setDatabaseName(path);    // set sqlite database file path
    bool connected = db_.open();  // open db connection
    if (!connected) {
      db_.close();  // close db connection
      return false;
    }
    return true;
}
bool SqliteStorage::close()
{
//    db_.close();
    return true;
}

// composers
Composer getComposerFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string fullname = query.value("fullname").toString().toStdString();
    int year = query.value("year").toInt();
    string composition = query.value("composition").toString().toStdString();
    int amount = query.value("amount").toInt();
    Composer c;
    c.id = id;
    c.fullname = fullname;
    c.year = year;
    c.composition =composition;
    c.amount = amount;
    return c;
}
vector<Composer> SqliteStorage::getAllComposers(void)
{
    vector<Composer> composers;
    QSqlQuery query("SELECT * FROM composers");
    while (query.next())
    {
        Composer c = getComposerFromQuery(query);
        composers.push_back(c);
    }
    return composers;

}
optional<Composer> SqliteStorage::getComposerById(int composer_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM composers WHERE id = :id");
    query.bindValue(":id", composer_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get composer error:" << query.lastError();
        return nullopt;
    }
    if (query.next()) {
        Composer c = getComposerFromQuery(query);
        return c;
    }
    return nullopt;
}
bool SqliteStorage::updateComposer(const Composer &composer)
{
    QSqlQuery query;
    query.prepare("UPDATE composers SET fullname = :fullname, year= :year, composition = :composition, amount = :amount WHERE id = :id");

    query.bindValue(":fullname", QString::fromStdString(composer.fullname));
    query.bindValue(":year", composer.year);
    query.bindValue(":composition", QString::fromStdString(composer.composition));
    query.bindValue(":amount", composer.amount);
    query.bindValue(":id", composer.id);
    if (!query.exec()){
        qDebug() << "updateComposer error:" << query.lastError();
        return false;
    }
    return true;
}
bool SqliteStorage::removeComposer(int composer_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM composers WHERE id = :id");
    query.bindValue(":id", composer_id);
    if (!query.exec()){
        qDebug() << "deleteComposer error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertComposer(const Composer & composer)
{
    QSqlQuery query;
    query.prepare("INSERT INTO composers (fullname, year, composition, amount)"
                  "VALUES (:fullname, :year, :composition, :amount)");
    query.bindValue(":fullname", QString::fromStdString(composer.fullname));
    query.bindValue(":year", composer.year);
    query.bindValue(":composition", QString::fromStdString(composer.composition));
    query.bindValue(":amount", composer.amount);
    if (!query.exec()){
        qDebug() << "addPerson error:"
                 << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
     qDebug() << "id : " << var.toString();
    return var.toInt();
}

// albums
Album getAlbumFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string composerName = query.value("composerName").toString().toStdString();
    string albumName = query.value("albumName").toString().toStdString();
    string kind = query.value("kind").toString().toStdString();
    int capacity = query.value("capacity").toInt();
    Album a;
    a.id = id;
    a.composerName = composerName;
    a.albumName = albumName;
    a.kind = kind;
    a.capacity= capacity;
    return a;
}
vector<Album> SqliteStorage::getAllAlbums(void)
{
    vector<Album> albums;
    QSqlQuery query("SELECT * FROM albums");
    while (query.next())
    {
        Album a = getAlbumFromQuery(query);
        albums.push_back(a);
    }
    return albums;
}
optional<Album> SqliteStorage::getAlbumById(int album_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM albums WHERE id = :id");
    query.bindValue(":id", album_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get album error:" << query.lastError();
        return nullopt;
    }
    if (query.next()) {
        Album a = getAlbumFromQuery(query);
        return a;
    }
    return nullopt;
}
bool SqliteStorage::updateAlbum(const Album &album)
{
    QSqlQuery query;
    query.prepare("UPDATE albums SET composerName = :composerName, albumName = :albumName, kind = :kind, capacity = :capacity WHERE id = :id");

    query.bindValue(":composerName", QString::fromStdString(album.composerName));
    query.bindValue(":albumName", QString::fromStdString(album.albumName));
    query.bindValue(":kind", QString::fromStdString(album.kind));
    query.bindValue(":capacity", album.capacity);

    query.bindValue(":id", album.id);

    if (!query.exec()){
        qDebug() << "updateAlbum error:" << query.lastError();
        return false;
    }
    return true;
}
bool SqliteStorage::removeAlbum(int album_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM albums WHERE id = :id");
    query.bindValue(":id", album_id);
    if (!query.exec()){
        qDebug() << "deleteAlbum error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertAlbum(const Album & album)
{
    QSqlQuery query;
    query.prepare("INSERT INTO albums (composerName, albumName, kind, capacity)"
                  "VALUES (:composerName, :albumName, :kind, :capacity)");

    query.bindValue(":composerName", QString::fromStdString(album.composerName));
    query.bindValue(":albumName", QString::fromStdString(album.albumName));
    query.bindValue(":kind", QString::fromStdString(album.kind));
    query.bindValue(":capacity", album.capacity);

    if (!query.exec()){
        qDebug() << "addAlbum error:" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
     qDebug() << "id : " << var.toString();
    return var.toInt();
}
