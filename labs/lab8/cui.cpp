#include "cui.h"
#include <QDebug>

using namespace std;
void printComposers(vector<Composer> &composers);
void printfStringsTable(CsvTable &table);
void printComposersMainInfo(vector<Composer> &composers);

void printAlbumsMainInfo(vector<Album> &al);
void printListOfAlbums(vector<Album> &al);

void mainMenu_print();
void printComposer(Composer &c);
void printAlbum(Album & al);


void Cui::show()
{
    int command = -1;
    system("clear");
    bool is_mainMenu_running = true;
    while (is_mainMenu_running)
    {
        is_mainMenu_running = false;
        puts("1. Composers Menu");
        puts("2. Albums Menu");
        puts("0. Exit ");
        cin.clear();
        cin >> command;

        switch (command)
        {
        case 1:
        {
            system("clear");
            composersMainMenu();
            break;
        };
        case 2:
        {
            system("clear");
            albumsMainMenu();
            break;
        }
        default:
        {
            is_mainMenu_running = false;
            break;
        }
        }
    }
}
//----------------------------------------

void Cui::composersMainMenu()
{
    int command = 0;
    bool is_running = true;

    while (is_running)
    {
        cout << endl;
        puts("1. Show all composers ");
        puts("2. Work with one composer");
        puts("3. Add new composer");
        puts("0. Return to the main menu");
        cin.clear();
        cin >> command;

        switch (command)
        {
        case 1:
        {
            //Show all composers from the file
            system("clear");

            cout << endl;
            auto cs = this->storage_->getAllComposers();
            printComposersMainInfo(cs);

            break;
        };
        case 2:
        {
            //Work with one composer
            system("clear");

            int comp_id = 0;
            cout << "Enter the id of composer you want to work with: " << endl;
            cin >> comp_id;
             system("clear");
            composerMenu(comp_id);

            break;
        }
        case 3:
        {
            //Add new composer
            system("clear");

            printf("Enter the new fullname: ");
            char name[100];
            cin.ignore();
            cin.getline(name, sizeof(name));

            printf("composition`s name:");
            char name_comp[100];
            cin.getline(name_comp, sizeof(name_comp));

            printf("year:");
            int year = 0;
            cin >> year;

            printf("amount:");
            int amount = 0;
            cin >> amount;

            Composer new_comp;

            new_comp.fullname = name;
            new_comp.composition = name_comp;
            new_comp.amount = amount;
            new_comp.year = year;

            this->storage_->insertComposer(new_comp);
            break;
        }
        default:
        {
            system("clear");
            is_running = false;
            break;
        }
        }
    }
}

void Cui::albumsMainMenu()
{
    int command = 0;
    bool is_running = true;

    while (is_running)
    {
        cout << endl;
        puts("1. Show all albums ");
        puts("2. Work with one album");
        puts("3. Add new album");
        puts("0. Return to the main menu");

        cin >> command;

        switch (command)
        {
        case 1:
        {
            //Show all albums from the file
            system("clear");
            cout << endl;
            auto cs = this->storage_->getAllAlbums();
            printAlbumsMainInfo(cs);

            break;
        };
        case 2:
        {
            //Work with one composer
            system("clear");

            int comp_id = 0;
            cout << "Enter the id of album you want to work with: " << endl;
            cin >> comp_id;
            system("clear");
            albumMenu(comp_id);

            break;
        }
        case 3:
        {
            //Add new album
            system("clear");

            printf("Composer name:");
            char comp_name[100];
            cin.ignore();
            cin.getline(comp_name, sizeof(comp_name));

            printf("album name: ");
            char album_name[100];
            cin.getline(album_name, sizeof(album_name));

            printf("kind:");
            char kind[100];
            cin.getline(kind, sizeof(kind));

            printf("capacity:");
            int capacity = 0;
            cin >> capacity;

            Album new_al;

            new_al.composerName = comp_name;
            new_al.albumName = album_name;
            new_al.kind = kind;
            new_al.capacity = capacity;

            this->storage_->insertAlbum(new_al);
            break;
        }
        default:
        {
            system("clear");
            is_running = false;
            break;
        }
        }
    }
}

void Cui::albumMenu(int entity_id)
{
    int command = 0;
    bool is_running = true;
    while (is_running)
    {
        cout << endl;

        puts("1. Detail information about albums ");
        puts("2. Detail information about this album ");
        puts("3. Update the album");
        puts("4. Delete the album");
        puts("0. Exit");
        cin >> command;

        switch (command)
        {
        case 1:
        {
            system("clear");
            vector<Album> v = this->storage_->getAllAlbums();
            printListOfAlbums(v);

            break;
        }
        case 2:
        {
            system("clear");

            optional <Album> opt = this->storage_->getAlbumById(entity_id);
            if(!opt)
            {
                qDebug() << "Not found";
            }else
            {
                Album c = opt.value();
                printAlbum(c);

            }
            break;
        }
        case 3:
        {
            system("clear");

            printf("New composer name:");
            char comp_name[100];
            cin.ignore();
            cin.getline(comp_name, sizeof(comp_name));

            printf("album name: ");
            char album_name[100];
            cin.getline(album_name, sizeof(album_name));

            printf("kind:");
            char kind[100];
            cin.getline(kind, sizeof(kind));

            printf("capacity:");
            int capacity = 0;
            cin >> capacity;

            Album new_al;

            new_al.id = entity_id;
            new_al.composerName = comp_name;
            new_al.albumName = album_name;
            new_al.kind = kind;
            new_al.capacity = capacity;

            this->storage_->updateAlbum(new_al);

            break;
        }
        case 4:
        {
            system("clear");
            this->storage_->removeAlbum(entity_id);
            is_running = false;

            system("clear");
        }
        default:
            system("clear");
            is_running = false;
            break;
        }
    }
}

void Cui::composerMenu(int entity_id)
{
    int command = 0;
    bool is_composerMenu_running = true;
    while (is_composerMenu_running)
    {
        cout << endl;

        puts("1. Detail information about composers ");
        puts("2. Detail information about this composer");
        puts("3. Update the composer");
        puts("4. Delete the composer");

        puts("0. Exit");
        cin >> command;

        switch (command)
        {
        case 1:
        {
            system("clear");
            vector<Composer> v = this->storage_->getAllComposers();
            printComposers(v);
            break;
        }
        case 2:
        {
            system("clear");

            optional <Composer> opt = this->storage_->getComposerById(entity_id);
            if(!opt)
            {
                qDebug() << "Not found";
            }else
            {
                Composer c = opt.value();
                printComposer(c);

            }
            break;
        }
        case 3:
        {
            system("clear");

            printf("Enter the new fullname: ");
            char name[100];
            cin.ignore();
            cin.getline(name, sizeof(name));

            printf("composition`s name:");
            char name_comp[100];
            cin.getline(name_comp, sizeof(name_comp));

            printf("year:");
            int year = 0;
            cin >> year;

            printf("amount:");
            int amount = 0;
            cin >> amount;

            Composer new_comp;

            new_comp.id = entity_id;
            new_comp.fullname = name;
            new_comp.composition = name_comp;
            new_comp.amount = amount;
            new_comp.year = year;

            this->storage_->updateComposer(new_comp);
            break;
        }
        case 4:
        {
            system("clear");
            this->storage_->removeComposer(entity_id);
            is_composerMenu_running = false;
            system("clear");
            break;
        }
        default:
        {
            system("clear");
            is_composerMenu_running = false;
            break;
        };
        }
    }
}

//----------------------------------------

void printComposer(Composer &c)
{
    cout << c.id << ",";
    cout << c.fullname << ",";
    cout << c.year << ",";
    cout << c.composition << ",";
    cout << c.amount << "," << endl;
}

void printComposersMainInfo(vector<Composer> &composers)
{
    for (Composer &c : composers)
    {
        cout << c.id << ",";
        cout << c.fullname << endl;
    }
}

void printAlbum(Album & al)
{
    cout << al.id << ",";
    cout << al.albumName << ",";
    cout << al.composerName << ",";
    cout << al.kind << ",";
    cout << al.capacity << endl;
}

void printListOfAlbums(vector<Album> &al)
{
    for (Album &c : al)
    {
        cout << c.id << ",";
        cout << c.albumName << ",";
        cout << c.composerName << ",";
        cout << c.kind << ",";
        cout << c.capacity << endl;
    }
}

void printAlbumsMainInfo(vector<Album> &al)
{
    for (Album &c : al)
    {
        cout << c.id << ",";
        cout << c.albumName << endl;
    }
}

void printComposers(vector<Composer> &composers)
{
    for (Composer &c : composers)
    {
        printComposer(c);
    }
}

void printfStringsTable(CsvTable &table)
{
    for (int i = 0; i < table.size(); i++)
    {
        CsvRow row = table[i];
        for (int j = 0; j < row.size(); j++)
        {
            if (i == 0 && j == 0)
            {
                cout << row[j];
            }
            else if (i != 0 && j == 0)
            {
                cout << row[j];
            }
            else
            {
                printf(",");
                cout << row[j];
            }
        }
        printf("\n");
    }
}

//----------------------------------------
