#include "xml_storage.h"
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QtXml>
#include <QDebug>
#include <string>

int XmlStorage::getNewComposerId(){
    int max_id = 0;
    for (Composer &c : this->composers_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
    return 0;
}
int XmlStorage::getNewAlbumId(){
    int max_id = 0;
    for (Album &c : this->albums_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
}

  QDomDocument readAllFromXmlFile(string const &filename)
  {
      QString  composers_filename = QString::fromStdString(filename);
      QFile file(composers_filename);

      bool is_composers_opened = file.open(QFile::ReadOnly);

      if (!is_composers_opened)
      {
          cerr << "Can`t open the file to read: " << filename;
          abort();
      }

      //
      QTextStream ts(&file);
      QString text = ts.readAll();
      QDomDocument doc;
      QString errorMessage;
      int errorLine;
      int errorColumn;

      bool is_passed = doc.setContent(text, &errorMessage, &errorLine, &errorColumn);
      if (!is_passed)
      {
          qDebug() << "Error parsing xml: " << errorMessage;
           qDebug() << "in line: " << errorLine;

      }
      file.close();
      return doc;
  }

Composer domElementToComposer(QDomElement & element)
{
    Composer c;
    c.id = element.attributeNode("id").value().toInt();
    c.fullname = element.attributeNode("fullname").value().toStdString();
    c.year = element.attributeNode("year").value().toInt();
    c.composition = element.attributeNode("composition").value().toStdString();
    c.amount = element.attributeNode("amount").value().toInt();

    return c;
}

Album domElementToAlbum(QDomElement & element)
{
    Album c;
    c.id = element.attributeNode("id").value().toInt();
    c.composerName = element.attributeNode("composerName").value().toStdString();
    c.albumName = element.attributeNode("albumName").value().toStdString();
    c.kind = element.attributeNode("kind").value().toStdString();
    c.capacity = element.attributeNode("capacity").value().toInt();

    return c;
}
//
bool XmlStorage::open(){

    std::string filenameComposer = this->dir_name_ + "/composers.xml";
    QDomDocument composers_doc = readAllFromXmlFile(filenameComposer);
    QDomElement composers_root = composers_doc.documentElement();

    for (int i = 0; i < composers_root.childNodes().size(); i++)
    {
        QDomNode composers_node = composers_root.childNodes().at(i);
        if(composers_node.isElement())
        {
            QDomElement composers_element = composers_node.toElement();
            Composer composer = domElementToComposer(composers_element);
            this->composers_.push_back(composer);
        }
    }

    //----
    std::string filenameAlbum = this->dir_name_ + "/albums.xml";
    QDomDocument albums_doc = readAllFromXmlFile(filenameAlbum);
    QDomElement albums_root = albums_doc.documentElement();

    for (int i = 0; i < albums_root.childNodes().size(); i++)
    {
        QDomNode albums_node = albums_root.childNodes().at(i);
        if(albums_node.isElement())
        {
            QDomElement albums_element = albums_node.toElement();
            Album album = domElementToAlbum(albums_element);
            this->albums_.push_back(album);
        }
    }

    //
    return true;
}

QDomElement composerToDomElement(QDomDocument & doc, Composer & c)
{
     QDomElement composer_el = doc.createElement("composer");
     composer_el.setAttribute("id", c.id);
     composer_el.setAttribute("fullname", c.fullname.c_str());
     composer_el.setAttribute("year", c.year);
     composer_el.setAttribute("composition", c.composition.c_str());
     composer_el.setAttribute("amount", c.amount);
     return composer_el;
}

QDomElement albumToDomElement(QDomDocument & doc, Album & a)
{
     QDomElement album_el = doc.createElement("composer");
     album_el.setAttribute("id", a.id);
     album_el.setAttribute("composerName", a.composerName.c_str());
     album_el.setAttribute("albumName", a.albumName.c_str());
     album_el.setAttribute("kind", a.kind.c_str());
     album_el.setAttribute("capacity", a.capacity);
     return album_el;
}

QString createXmlComposers_Text(vector<Composer> & composers)
{
    QDomDocument composers_doc;
    QDomElement composers_root = composers_doc.createElement("composers");
    for (Composer & composer : composers)
    {
       QDomElement composer_el = composerToDomElement(composers_doc, composer);
        composers_root.appendChild(composer_el);
    }
    composers_doc.appendChild(composers_root);
    QString composers_xml_text = composers_doc.toString(4);
    return composers_xml_text;
}

QString createXmlAlbums_Text(vector<Album> & albums)
{
    QDomDocument albums_doc;
    QDomElement albums_root = albums_doc.createElement("albums");
    for (Album & album: albums)
    {
       QDomElement albums_el = albumToDomElement(albums_doc, album);
        albums_root.appendChild(albums_el);
    }
    albums_doc.appendChild(albums_root);
    QString albums_xml_text = albums_doc.toString(4);
    return albums_xml_text;
}

void writeXmlTextToFile(string const &filename, QString & xml_text)
{
    QString  mainFilename = QString::fromStdString(filename);

    QFile file(mainFilename);
    if (!file.open(QFile::WriteOnly))
    {
        qDebug() << "Can`t open file to write: " << mainFilename;
        abort();
    }

    QTextStream ts(&file);
    ts << xml_text;
    file.close();
}

bool XmlStorage::close(){ //write to File
    //
    QString composers_xml_text = createXmlComposers_Text(this->composers_);
    std::string filenameComp = this->dir_name_ + "/composers.xml";
    writeXmlTextToFile(filenameComp, composers_xml_text);
    //============
    QString albums_xml_text = createXmlAlbums_Text(this->albums_);
    std::string filenameAlb = this->dir_name_ + "/albums.xml";
    writeXmlTextToFile(filenameAlb, albums_xml_text);

    return true;
}

// Composers
vector<Composer> XmlStorage::getAllComposers(void){
    return this->composers_;
}
optional<Composer> XmlStorage::getComposerById(int composer_id){
    for (Composer &c : this->composers_)
    {
        if (c.id == composer_id)
        {
            return c;
        }
    }
    return nullopt;
}
bool XmlStorage::updateComposer(const Composer &composer){
    for (Composer &c : this->composers_)
    {
        if (c.id == composer.id)
        {
            c.fullname = composer.fullname;
            c.composition = composer.composition;
            c.year = composer.year;
            c.amount = composer.amount;
            return true;
        }
    }
    return false;
}
bool XmlStorage::removeComposer(int composer_id){
    int index = -1;
    for (int i = 0; i < this->composers_.size(); i++)
    {
        if (this->composers_[i].id == composer_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->composers_.erase(this->composers_.begin() + index);
        return true;
    }
    return false;
}
int XmlStorage::insertComposer(const Composer &composer){
    int new_id = this->getNewComposerId();
    Composer copy = composer;
    copy.id = new_id;
    this->composers_.push_back(copy);
    return new_id;
}

//albums
vector<Album> XmlStorage::getAllAlbums(void){
    return this->albums_;
}
optional<Album> XmlStorage::getAlbumById(int album_id){
    for (Album &c : this->albums_)
    {
        if (c.id == album_id)
        {
            return c;
        }
    }
    return nullopt;
}
bool XmlStorage::updateAlbum(const Album &album){
    for (Album &c : this->albums_)
    {
        if (c.id == album.id)
        {
            c.composerName = album.composerName;
            c.albumName = album.albumName;
            c.capacity = album.capacity;
            c.kind = album.kind;
            return true;
        }
    }
    return false;
}
bool XmlStorage::removeAlbum(int album_id){
    int index = -1;
    for (int i = 0; i < this->albums_.size(); i++)
    {
        if (this->albums_[i].id == album_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->albums_.erase(this->albums_.begin() + index);
        return true;
    }
    return false;
}
int XmlStorage::insertAlbum(const Album &album){
    int new_id = this->getNewAlbumId();
    Album copy = album;
    copy.id = new_id;
    this->albums_.push_back(copy);
    return new_id;
}
