#include "csv.h"
#include "composer.h"
#include <ctype.h>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>

#include "csv_storage.h"
#include "xml_storage.h"
#include "sqlite_storage.h"

#include "cui.h"

using namespace std;

int main()
{
    SqliteStorage sqlite_storage("../lab8/data/sql");

    Storage * storage_ptr = &sqlite_storage;
    storage_ptr->open();

    Cui cui(storage_ptr);
    cui.show();
    storage_ptr->close();
    return 0;
}
