#pragma once

#include <vector>
#include <string>

#include "storage.h"

#include "optional.h"
#include "composer.h"
#include "album.h"
#include "csv.h"

using std::string;
using std::vector;

class CsvStorage : public Storage
{
  const string dir_name_;

  vector<Composer> composers_;
  vector<Album> albums_;

  static Composer rowToComposer(const CsvRow &row);
  static CsvRow composerToRow(const Composer &st);

  int getNewComposerId();
  int getNewAlbumId();

public:
  CsvStorage(const string &dir_name) : dir_name_(dir_name) {}

  bool open();
  bool close();
  
  // Composers
  virtual vector<Composer> getAllUserComposers(int user_id) = 0;
  vector<Composer> getAllComposers(void);
  optional<Composer> getComposerById(int composer_id);
  bool updateComposer(const Composer &composer);
  bool removeComposer(int composer_id);
  int insertComposer(const Composer &composer);

  //albums
  vector<Album> getAllAlbums(void);
  optional<Album> getAlbumById(int album_id);
  bool updateAlbum(const Album &album);
  bool removeAlbum(int album_id);
  int insertAlbum(const Album & album);

  //users
  optional<User> getUserAuth(string & username, string & password);

  // links
  vector<Album> getAllComposerAlbums(int composer_id);
  bool insertComposerAlbum(int composer_id, int album_id);
  bool removeComposerAlbum(int composer_id, int album_id);
};
