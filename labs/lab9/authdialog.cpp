#include "authdialog.h"
#include "ui_authdialog.h"
#include <QCryptographicHash>
#include <QDebug>
AuthDialog::AuthDialog(User *user, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog)
{
    ui->setupUi(this);
    change = user;
    ui->userAuth_label->setText("<b> Enter your data to log in: <b/>");
}

AuthDialog::~AuthDialog()
{
    delete ui;
}

void AuthDialog::on_buttonBox_accepted()
{
    qDebug() << "1";
    change->username = ui->username_lineEdit->text().toStdString();
    qDebug() << "2";
    string entered_password = ui->password_lineEdit->text().toStdString();
    QString hash = QString(QCryptographicHash::hash((entered_password.c_str()),QCryptographicHash::Md5).toHex());
    qDebug() << "hash";
    change->password_hash = hash.toStdString();
}
