#include "dialog.h"
#include "ui_dialog.h"
#include <QWidget>
#include <QDebug>
#include <QSpinBox>

Q_DECLARE_METATYPE(Composer*)

Dialog::Dialog(Composer *composer, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    change = composer;
    ui->newComposer_label->setText("<b> Add new composer: <b/>");
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_buttonBox_accepted()
{
    change->fullname = ui->fullname_lineEdit->text().toStdString();
    change->composition = ui->composition_LineEdit->text().toStdString();
    change->year = ui->year_spinBox->text().toInt();
    change->amount = ui->amount_spinBox->text().toInt();

}
