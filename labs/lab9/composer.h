#pragma once

#include <string>

using std::string;

struct Composer
{
    int id;
    string fullname;
    int year;
    string composition;
    int amount;
};
