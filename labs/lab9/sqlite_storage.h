#pragma once
#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H


#include "storage.h"
#include <QSqlDatabase>

class SqliteStorage: public Storage
{
    const string dir_name_;
    int userId;
    QSqlDatabase db_;

public:
    SqliteStorage(const string &dir_name);

     bool open();
     bool close();

    // composers
     vector<Composer> getAllUserComposers(int user_id);
     vector<Composer> getAllComposers(void);
     optional<Composer> getComposerById(int composer_id);
     bool updateComposer(const Composer &composer);
     bool removeComposer(int composer_id);
     int insertComposer(const Composer & composer);

    // albums
     vector<Album> getAllAlbums(void);
     optional<Album> getAlbumById(int album_id);
     bool updateAlbum(const Album &album);
     bool removeAlbum(int album_id);
     int insertAlbum(const Album & album);

     //users
     optional<User> getUserAuth(string & username, string & password);

     // links
     vector<Album> getAllComposerAlbums(int composer_id);
     bool insertComposerAlbum(int composer_id, int album_id);
     bool removeComposerAlbum(int composer_id, int album_id);
};

#endif // SQLITE_STORAGE_H
