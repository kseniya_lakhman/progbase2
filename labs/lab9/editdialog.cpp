#include "editdialog.h"
#include "ui_editdialog.h"
#include <QWidget>
#include <QDebug>


Q_DECLARE_METATYPE(Composer*)
Q_DECLARE_METATYPE(Album)

EditDialog::EditDialog(Composer *composer, Storage *storage, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);
    change = composer;
    ui->newComposer_label->setText("<b> Edit new composer: <b/>");
    stor = storage;
    setWidgetComposerAlbumsList(stor->getAllComposerAlbums(composer->id));
    setWidgetAllAlbumsList();
}

EditDialog::~EditDialog()
{
    delete ui;
}

void EditDialog::setWidgetAllAlbumsList()
{
    vector <Album> composer_albums = this->stor->getAllComposerAlbums(change->id);
    vector <Album> all_albums = this->stor->getAllAlbums();

    foreach(Album al, all_albums)
    {
        bool isCompNotAdded= false;

        foreach(Album album, composer_albums)
        {
            if(album.id == al.id)
            {
                isCompNotAdded = true;
            }

        }

        if(!isCompNotAdded)
        {
            QVariant var;
            var.setValue(al);

            QListWidgetItem * newAlbumListItem = new QListWidgetItem();
            newAlbumListItem->setText(al.albumName.c_str());
            newAlbumListItem->setData(Qt::UserRole, var);
            ui->allALbums_listWidget->addItem(newAlbumListItem);
        }

    }
}

void EditDialog::setWidgetComposerAlbumsList(vector <Album> albums)
{
    foreach(Album al, albums)
    {
        QVariant var;
        var.setValue(al);

        QListWidgetItem * newAlbumListItem = new QListWidgetItem();
        newAlbumListItem->setText(al.albumName.c_str());
        newAlbumListItem->setData(Qt::UserRole, var);
        ui->compAlbums_listWidget->addItem(newAlbumListItem);
    }
}

void EditDialog::on_buttonBox_accepted()
{
    change->fullname = ui->fullname_lineEdit->text().toStdString();
    change->composition = ui->composition_LineEdit->text().toStdString();
    change->year = ui->year_spinBox->text().toInt();
    change->amount = ui->amount_spinBox->text().toInt();
}

void EditDialog::presentValuesInEditDialog()
{
    ui->fullname_lineEdit->setText(QString::fromStdString(change->fullname));
    ui->composition_LineEdit->setText(QString::fromStdString(change->composition));
    ui->year_spinBox->setValue(change->year);
    ui->amount_spinBox->setValue(change->amount);
}

void EditDialog::on_add_pushButton_clicked()
{
    QList<QListWidgetItem *> item = ui->allALbums_listWidget->selectedItems();
    if(item.count() == 1)
    {
        QListWidgetItem * selectedAlbum = item.at(0);
        QVariant var = selectedAlbum->data(Qt::UserRole);
        Album * album= static_cast<Album*>(var.data());

        this->stor->insertComposerAlbum(change->id, album->id);

        QListWidgetItem * newComposerListItem = new QListWidgetItem();
        newComposerListItem->setText(album->albumName.c_str());
        newComposerListItem->setData(Qt::UserRole, var);
        ui->compAlbums_listWidget->addItem(newComposerListItem);

        int row_index = ui->allALbums_listWidget->row(selectedAlbum);
        ui->allALbums_listWidget->takeItem(row_index);

        delete  selectedAlbum;
    }
}


void EditDialog::on_delete_pushButton_clicked()
{
    QList<QListWidgetItem *> item = ui->compAlbums_listWidget->selectedItems();
    if(item.count() == 1)
    {
        QListWidgetItem * selectedAlbum = item.at(0);
        QVariant var = selectedAlbum->data(Qt::UserRole);
        Album * album= static_cast<Album*>(var.data());

        QListWidgetItem * newComposerListItem = new QListWidgetItem();
        newComposerListItem->setText(album->albumName.c_str());
        newComposerListItem->setData(Qt::UserRole, var);
        ui->allALbums_listWidget->addItem(newComposerListItem);

        int row_index = ui->compAlbums_listWidget->row(selectedAlbum);
        ui->compAlbums_listWidget->takeItem(row_index);

        this->stor->removeComposerAlbum(change->id, album->id);
        delete  selectedAlbum;
    }

}



void EditDialog::on_allALbums_listWidget_itemClicked(QListWidgetItem *item)
{
    qDebug() << "item clicked on_allALbums! ";
}


void EditDialog::on_compAlbums_listWidget_itemClicked(QListWidgetItem *item)
{
       qDebug() << "item clicked on_compAlbums! ";
}
