#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>
#include "user.h"

namespace Ui {
class AuthDialog;
}

class AuthDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AuthDialog(User *user, QWidget *parent = 0);
    ~AuthDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::AuthDialog *ui;
    User * change;
};

#endif // AUTHDIALOG_H
