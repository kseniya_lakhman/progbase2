#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "editdialog.h"
#include "authdialog.h"
#include "composer.h"

#include "storage.h"
#include "xml_storage.h"
#include "sqlite_storage.h"

#include <QListWidgetItem>
#include <QFileDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QPalette>
Q_DECLARE_METATYPE(Composer*)
Q_DECLARE_METATYPE(Composer)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->composersLabel->setText("<b>Composers:</b>");
    ui->selectedComposerLabel->setText("<b>Selected composer:</b>");
    this->setWindowTitle("Composer Database");

    if (this->storage == nullptr)
    {
        ui->addButton->setEnabled(false);
        ui->editButton->setEnabled(false);
        ui->removeButton->setEnabled(false);
    }

    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::beforeExit);
    connect(ui->actionNew_storage, &QAction::triggered, this, &MainWindow::saveNewStorage);
    connect(ui->actionOpen_Storage, &QAction::triggered, this, &MainWindow::openNewStorage);


}

MainWindow::~MainWindow()
{
    this->storage->close();
    delete ui;
}

void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.count() == 0)
    {
        qDebug() << "nothing to remove";
    }
    else
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
            this,
            "On delete",
            "Are you sure?",
            QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {

            foreach(QListWidgetItem * selectedItem, items)
            {
                int row_index = ui->listWidget->row(selectedItem);
                ui->listWidget->takeItem(row_index);
                QVariant var = selectedItem->data(Qt::UserRole);
                Composer *comp = static_cast<Composer *>(var.data());

                this->storage->removeComposer(comp->id);
                delete selectedItem;
            }

        } else {
            qDebug() << "Yes was *not* clicked";
        }
    }
//    this->storage->save();
}
User MainWindow::logIn()
{
    User user;
    AuthDialog authDialog(&user);
    authDialog.setWindowTitle("Log in");
    int status = authDialog.exec();
    if (status == 1)
    {
      qDebug() << "Accepted: " << user.username.c_str() << " - " << user.password_hash.c_str();

      if( (this->storage->getUserAuth(user.username, user.password_hash)) != nullopt)
      {
          QMessageBox::information(
              this,
              "Log in",
              "Successfull!");
          user = *this->storage->getUserAuth(user.username, user.password_hash);
          return user;
      }else
      {
          QMessageBox::information(
              this,
              "Log in",
              "Incorrect username or password");
      }
     } else {
              qDebug() << "Rejected!";
     }
    user.id = -1;
    return user;
}

void MainWindow::beforeExit()
{
   QMessageBox::StandardButton reply;
   reply = QMessageBox::question(
       this,
       "On delete",
       tr("Are you sure?"),
       QMessageBox::Yes|QMessageBox::No);
   if (reply == QMessageBox::Yes) {
       qDebug() << "Yes was clicked";
//       this->storage->close();
       this->close();
   } else {
       qDebug() << "Yes was *not* clicked";
   }
}

void MainWindow::on_addButton_clicked() // @TODO
{
    Composer comp;
    Dialog addDialog(&comp);
    addDialog.setWindowTitle("Add");
    int status = addDialog.exec();

    if (status == 1 && comp.fullname.size() != 0 && comp.composition.size() != 0) {
      qDebug() << "Accepted!";
      int id = this->storage->insertComposer(comp);
      comp.id = id;

      QVariant var;
      var.setValue(comp);

      QListWidgetItem * newComposerListItem = new QListWidgetItem();
      newComposerListItem->setText(comp.fullname.c_str());
      newComposerListItem->setData(Qt::UserRole, var);
      ui->listWidget->addItem(newComposerListItem);
     } else {
              qDebug() << "Rejected!";
     }
}

Composer copy(Composer * composer)
{
    Composer c;

    c.id = composer->id;
    c.fullname = composer->fullname;
    c.composition = composer->composition;
    c.year = composer->year;
    c.amount = composer->amount;

    return c;

}

void MainWindow::on_editButton_clicked() //@TODO
{
    QList<QListWidgetItem *> item = ui->listWidget->selectedItems();
    QListWidgetItem * selectedComposer = item.at(0);
    QVariant var = selectedComposer->data(Qt::UserRole);
    Composer * comp = static_cast<Composer*>(var.data());
    vector <Album> albums;
    albums = this->storage->getAllComposerAlbums(comp->id);

    EditDialog editDialog(comp, this->storage);
    editDialog.setWindowTitle("Edit");
    editDialog.presentValuesInEditDialog();
    int status = editDialog.exec();

    if (status == 1 && comp->fullname.size() != 0 && comp->composition.size() != 0)
    {
      qDebug() << "Accepted!";
      var.setValue(copy(comp));

      selectedComposer->setText(comp->fullname.c_str());
      selectedComposer->setData(Qt::UserRole, var);

      this->storage->updateComposer(copy(comp));
//      this->storage->save();

    } else
    {
        qDebug() << "Rejected!";
    }
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    bool isOnlyOneItemSelected= false;
    bool isSelected = false;
    if (ui->listWidget->selectedItems().size() >= 1)
    {
        isSelected = true;
        if(ui->listWidget->selectedItems().size() == 1)
        {
            isOnlyOneItemSelected = true;
        }
    }

    ui->editButton->setEnabled(isOnlyOneItemSelected);
    ui->removeButton->setEnabled(isSelected);
    if (item != nullptr)
    {
        QVariant var = item->data(Qt::UserRole);
        Composer *comp = static_cast<Composer*>(var.data());
        ui->fullname_labelOut->setText(QString::fromStdString(comp->fullname));
        ui->composition_labelOut->setText(QString::fromStdString(comp->composition));
        ui->year_labelOut->setText(QString::number(comp->year));
        ui->amount_labelOut->setText(QString::number(comp->amount));

        vector <Album> albums = this->storage->getAllComposerAlbums(comp->id);
        if (albums.size() != 0)
        {
            string al_name = albums.at(0).albumName;
            qDebug() << "----------------------------------" << al_name.c_str();
            QString string = QString::fromStdString(al_name);
            QString nums = QString::number(albums.size()-1);
            if(string.size() != 0)
            {
                ui->albumOut_label->setText(string + "(+" + nums + ")");
            }
        }else
        {
            ui->albumOut_label->clear();
        }    }
}

void MainWindow::saveNewStorage() //save
{
       QFileDialog dialog(this);
       dialog.setFileMode(QFileDialog::Directory);
       QString current_dir = QDir::currentPath();
       QString default_name = "new_storage";

       QString folder_path = dialog.getSaveFileName(
           this,
           "Select New Storage Folder",
           current_dir + "/" + default_name,
           "Folders");
       if (folder_path != nullptr)
       {
          ui->listWidget->clear();
//          XmlStorage * xml_storage = new XmlStorage(folder_path.toStdString());
//          this->storage = xml_storage;
//          this->storage->save();
//          this->storage->load();
          ui->addButton->setEnabled(true);
       }
}

void MainWindow::setGUIFromOpenFile(vector <Composer> composers)
{
    foreach(Composer c, composers)
    {
            QVariant var;
            var.setValue(c);

            QListWidgetItem * newComposerListItem = new QListWidgetItem();
            newComposerListItem->setText(c.fullname.c_str());
            newComposerListItem->setData(Qt::UserRole, var);
            ui->listWidget->addItem(newComposerListItem);
    }
}

void MainWindow::openNewStorage()
{

   QString fileName = QFileDialog::getOpenFileName(
               this,              // parent
               "Open",  // caption
               "../lab9/data/sql",                // directory to start with
               "SQL (*.sqlite);;All Files (*)");  // file name filter
   qDebug() << fileName;
   if (fileName != nullptr)
   {
       ui->listWidget->clear();

       SqliteStorage * sqlite_storage = new SqliteStorage(fileName.toStdString());
       this->storage = sqlite_storage;
       this->storage->open();
       //
       User user = MainWindow::logIn();
       //
        if (user.id != -1)
        {
            setGUIFromOpenFile(sqlite_storage->getAllUserComposers(user.id));
            ui->addButton->setEnabled(true);
            ui->usernameOut_label->setText(QString::fromStdString(user.username));
        }
   }

}
