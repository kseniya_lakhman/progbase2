#include "storage.h"

class Cui
{
    Storage *const storage_;

    // composers menus
    void composersMainMenu();
    void composerMenu(int entity_id);
    void composerUpdateMenu(int entity_id);
    void composerDeleteMenu(int entity_id);
    void composerCreateMenu();

    void albumsMainMenu();
    void albumMenu(int entity_id);

  public:
    Cui(Storage *storage) : storage_(storage) {}
    //
    void show();
};