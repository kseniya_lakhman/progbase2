
#include <iostream>

#include <cstring>
#include <string>

using namespace std;

struct Album
{
    int id;
    string composerName;
    string albumName;
    string kind;
    int capacity;
};