#include "cui.h"
#include <iostream>

using namespace std;
void printComposers(vector<Composer> &composers);
void printfStringsTable(CsvTable &table);
void printComposersMainInfo(vector<Composer> &composers);

void printAlbumsMainInfo(vector<Album> &al);
void printListOfAlbums(vector<Album> &al);

void mainMenu_print();

void Cui::show()
{
    int command = -1;
    system("clear");

    // auto alb = this->storage_->getAllAlbums();

    bool is_mainMenu_running = true;
    while (is_mainMenu_running)
    {

        puts("1. Composers Menu");
        puts("2. Albums Menu");
        puts("0. Exit ");
        cin.clear();
        cin >> command;

        switch (command)
        {
        case 1:
        {
            system("clear");
            composersMainMenu();
            break;
        };
        case 2:
        {
            system("clear");
            albumsMainMenu();
            break;
        }
        default:
        {
            // system("clear");
            is_mainMenu_running = false;
            break;
        }
        }
    }
}

//----------------------------------------

//----------------------------------------

void Cui::composersMainMenu()
{
    int command = 0;
    bool is_running = true;

    while (is_running)
    {
        cout << endl;
        puts("1. Show all composers ");
        puts("2. Work with one composer");
        puts("3. Add new composer");
        puts("0. Return to the main menu");
        cin.clear();
        cin >> command;

        switch (command)
        {
        case 1:
        {
            //Show all composers from the file

            system("clear");

            cout << endl;
            string text_str = this->storage_->readAllFromFile("./data/data.csv");
            CsvTable csv_table = Csv::createTableFromString(text_str);
            auto cs = this->storage_->getAllComposers();
            printComposersMainInfo(cs);

            break;
        };
        case 2:
        {
            //Work with one composer
            system("clear");

            int comp_id = 0;
            cout << "Enter the id of composer you want to work with: " << endl;
            cin >> comp_id;
            system("clear");
            composerMenu(comp_id);

            break;
        }
        case 3:
        {
            //Add new composer
            system("clear");

            printf("Enter the new fullname: ");
            string name;
            cin.ignore();
            getline(cin, name);

            printf("composition`s name:");
            string name_comp;

            cin.ignore();
            getline(cin, name_comp);

            printf("year:");
            int year = 0;
            cin >> year;

            printf("amount:");
            int amount = 0;
            cin >> amount;

            Composer new_comp;

            new_comp.fullname = name;
            new_comp.composition = name_comp;
            new_comp.amount = amount;
            new_comp.year = year;

            this->storage_->insertComposer(new_comp);
            this->storage_->save();

            break;
        }
        default:
        {
            system("clear");
            is_running = false;
            break;
        }
        }
    }
}

void Cui::albumsMainMenu()
{
    int command = 0;
    bool is_running = true;

    while (is_running)
    {
        cout << endl;
        puts("1. Show all albums ");
        puts("2. Work with one album");
        puts("3. Add new album");
        puts("0. Return to the main menu");

        cin >> command;

        switch (command)
        {
        case 1:
        {
            //Show all composers from the file

            system("clear");

            cout << endl;
            string text_str = this->storage_->readAllFromFile("./data/albums.csv");
            CsvTable csv_table = Csv::createTableFromString(text_str);
            auto cs = this->storage_->getAllAlbums();
            printAlbumsMainInfo(cs);

            break;
        };
        case 2:
        {
            //Work with one composer
            system("clear");

            int comp_id = 0;
            cout << "Enter the id of album you want to work with: " << endl;
            cin >> comp_id;
            system("clear");
            albumMenu(comp_id);

            break;
        }
        case 3:
        {
            //Add new album
            system("clear");

            printf("composer name:");
            string comp_name;
            cin.ignore();
            getline(cin, comp_name);

            printf("Enter the new album name: ");
            string album_name;
            cin.ignore();
            getline(cin, album_name);

            printf("kind:");
            string kind;
            cin.ignore();
            getline(cin, kind);

            printf("capacity:");
            int capacity = 0;
            cin >> capacity;

            Album new_al;

            new_al.composerName = comp_name;
            new_al.albumName = album_name;
            new_al.kind = kind;
            new_al.capacity = capacity;

            this->storage_->insertAlbum(new_al);
            this->storage_->save();

            break;
        }
        default:
        {
            system("clear");
            is_running = false;
            break;
        }
        }
    }
}

void Cui::albumMenu(int entity_id)
{
    int command = 0;
    bool is_running = true;
    while (is_running)
    {
        cout << endl;

        puts("1. Detail information about albums ");
        puts("2. Update the album");
        puts("3. Delete the album");
        puts("0. Exit");
        cin >> command;

        switch (command)
        {
        case 1:
        {
            system("clear");
            vector<Album> v = this->storage_->getAllAlbums();
            printListOfAlbums(v);

            break;
        }
        case 2:
        {
            system("clear");

            printf("composer name:");
            string comp_name;
            cin >> comp_name;

            printf("Enter the new album name: ");
            string album_name;
            cin >> album_name;

            printf("kind:");
            string kind;
            cin >> kind;

            printf("capacity:");
            int capacity = 0;
            cin >> capacity;

            Album new_al;

            new_al.id = entity_id;
            new_al.composerName = comp_name;
            new_al.albumName = album_name;
            new_al.kind = kind;
            new_al.capacity = capacity;

            this->storage_->updateAlbum(new_al);
            this->storage_->save();
            break;
        }
        case 3:
        {
            system("clear");
            this->storage_->removeAlbum(entity_id);
            is_running = false;
            this->storage_->save();
            system("clear");
        }
        default:
            system("clear");
            is_running = false;
            break;
        }
    }
}

void Cui::composerMenu(int entity_id)
{
    int command = 0;
    bool is_composerMenu_running = true;
    while (is_composerMenu_running)
    {
        cout << endl;

        puts("1. Detail information about composers ");
        puts("2. Update the composer");
        puts("3. Delete the composer");
        puts("0. Exit");
        cin >> command;

        switch (command)
        {
        case 1:
        {
            system("clear");
            vector<Composer> v = this->storage_->getAllComposers();
            printComposers(v);

            break;
        }
        case 2:
        {
            system("clear");

            Composer new_comp;
            printf("Enter the fullname: ");
            string name;
            cin >> name;

            printf("composition`s name:");
            string name_comp;
            cin >> name_comp;

            printf("year:");
            int year = 0;
            cin >> year;

            printf("amount:");
            int amount = 0;
            cin >> amount;

            new_comp.id = entity_id;
            new_comp.fullname = name;
            new_comp.composition = name_comp;
            new_comp.amount = amount;
            new_comp.year = year;

            this->storage_->updateComposer(new_comp);
            this->storage_->save();
            break;
        }
        case 3:
        {
            system("clear");
            this->storage_->removeComposer(entity_id);
            is_composerMenu_running = false;
            this->storage_->save();
            system("clear");
        }
        default:
        {
            system("clear");
            is_composerMenu_running = false;
            break;
        };
        }
    }
}

//----------------------------------------

void printComposer(Composer &c)
{
    cout << c.id << ",";
    cout << c.fullname << ",";
    cout << c.year << ",";
    cout << c.composition << ",";
    cout << c.amount << "," << endl;
}

void printComposersMainInfo(vector<Composer> &composers)
{
    for (Composer &c : composers)
    {
        cout << c.id << ",";
        cout << c.fullname << endl;
    }
}

void printListOfAlbums(vector<Album> &al)
{
    for (Album &c : al)
    {
        cout << c.id << ",";
        cout << c.albumName << ",";
        cout << c.composerName << ",";
        cout << c.kind << ",";
        cout << c.capacity << endl;
    }
}

void printAlbumsMainInfo(vector<Album> &al)
{
    for (Album &c : al)
    {
        cout << c.id << ",";
        cout << c.albumName << endl;
    }
}

void printComposers(vector<Composer> &composers)
{
    for (Composer &c : composers)
    {
        printComposer(c);
    }
}

void printfStringsTable(CsvTable &table)
{
    for (int i = 0; i < table.size(); i++)
    {
        CsvRow row = table[i];
        for (int j = 0; j < row.size(); j++)
        {
            if (i == 0 && j == 0)
            {
                cout << row[j];
            }
            else if (i != 0 && j == 0)
            {
                cout << row[j];
            }
            else
            {
                printf(",");
                cout << row[j];
            }
        }
        printf("\n");
    }
}

//----------------------------------------