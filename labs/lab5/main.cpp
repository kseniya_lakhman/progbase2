#include "csv.h"
#include "composer.h"
#include <ctype.h>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>

#include "storage.h"
#include "cui.h"

using namespace std;

int main(int argc, char *argv[])
{
    system("clear");
    Storage storage("./data");
    storage.load();

    Cui cui(&storage);
    cui.show();
    return 0;
}
